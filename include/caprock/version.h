/*
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_VERSION_H
#define CAPROCK_VERSION_H

#include <caprock.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * The library's major, minor and patch version numbers are provided in this
 * struct.
 */
struct caprock_version_data
{
  uint16_t major;
  uint16_t minor;
  uint16_t patch;
};

/**
 * Return the library's version number.
 */
CAPROCK_API struct caprock_version_data caprock_version();

/**
 * Return a copyright string for this library; the returned value must not be
 * freed.
 */
CAPROCK_API char const * caprock_copyright_string();

/**
 * Return a license string for this library; the returned value must not be
 * freed.
 */
CAPROCK_API char const * caprock_license_string();

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
