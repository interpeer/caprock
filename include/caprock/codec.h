/*
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_CODEC_H
#define CAPROCK_CODEC_H

#include <caprock.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * This file defines codec/serialization constants. These details do not need
 * to be part of the public API, but may help compatible implementations.
 */


/**
 * Each token field is self-describing, and starts with a variable integer
 * type field. The variable integer encoding is the one used by interpeer's
 * liberate library.
 *
 * Field identifiers are individually versioned for backwards compatibility.
 *
 * Below are the field identifiers defined. Their values and meanings are
 * taken from <a
 *  href="https://specs.interpeer.io/draft-jfinkhaeuser-caprock-enc-compact/"
 *    >draft-jfinkhaeuser-caprock-enc-compact</a>.
 */
typedef enum
{
  // Token
  CFI_TOKENv1               = 0x0020,
  CFI_TOKEN_TYPEv1          = 0x0024,

  // Issuer tag
  CFI_ISSUER_IDv1           = 0x0028,

  // Token sequence number
  CFI_SEQUENCE_NOv1         = 0x002c,

  // Scope, including subfields.
  CFI_SCOPEv1               = 0x0030,
  CFI_SCOPEv1_FROM          = 0x0034,
  CFI_SCOPEv1_TO            = 0x0040,
  CFI_SCOPEv1_EXPIRY_POLICY = 0x0044,

  // Claims, including subfields
  CFI_CLAIMSv1              = 0x0048,
  CFI_CLAIMv1_SUBJECT       = 0x004c,
  CFI_CLAIMv1_PREDICATE     = 0x0050,
  CFI_CLAIMv1_PRIVILEGE     = CFI_CLAIMv1_PREDICATE,
  CFI_CLAIMv1_OBJECT        = 0x0054,

  // Identifiers
  CFI_IDv1_NONE             = 0x0008,
  CFI_IDv1_WILDCARD         = 0x000c,
  CFI_IDv1_RAW_32           = 0x0005,
  CFI_IDv1_RAW_57           = 0x001d,
  CFI_IDv1_SHA3_28          = 0x0003,
  CFI_IDv1_SHA3_32          = 0x0007,
  CFI_IDv1_SHA3_48          = 0x0017,
  CFI_IDv1_SHA3_64          = 0x0027,

  // Signatures
  CFI_SIGv1_RAW_32          = 0x0045,
  CFI_SIGv1_RAW_57          = 0x005d,
  CFI_SIGv1_SHA2_28         = 0x0042,
  CFI_SIGv1_SHA2_32         = 0x0046,
  CFI_SIGv1_SHA2_48         = 0x0056,
  CFI_SIGv1_SHA2_64         = 0x0066,
  CFI_SIGv1_SHA3_28         = 0x0043,
  CFI_SIGv1_SHA3_32         = 0x0047,
  CFI_SIGv1_SHA3_48         = 0x0057,
  CFI_SIGv1_SHA3_64         = 0x0067,
} caprock_field_identifier;


/**
 * Caprock specifies grant and revoke tokens for the time being.
 */
typedef enum
{
  CTT_GRANT   = 0x00,
  CTT_REVOKE  = 0x01,
} caprock_token_type;


/**
 * The scope expiry policy is either determined strictly by the issuer, or
 * according to local policy.
 */
typedef enum
{
  CEP_ISSUER  = 0x00,
  CEP_LOCAL   = 0x01,
} caprock_expiry_policy;


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
