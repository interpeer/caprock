/*
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_BASICS_H
#define CAPROCK_BASICS_H

/** @file */

#include <caprock.h>

#include <caprock/keys.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * As the file name suggests, this function contains functions for basic
 * caprock functionality. Functions here are public, but it is not advisable
 * to use them directly - other APIs are much more convenient to use. However,
 * they make for good testing of fundamentals.
 *
 * An expiry policy can take the \ref CAPROCK_EXPIRY_POLICY_ISSUER value to
 * show that the issuer's own expiry policy should be respected.
 */
CAPROCK_API extern char const * const CAPROCK_EXPIRY_POLICY_ISSUER;

/**
 * The \ref CAPROCK_EXPIRY_POLICY_LOCAL is weaker than
 * \ref CAPROCK_EXPIRY_POLICY_ISSUER, and permits the system to override the
 * issuer's expiry policy with a local policy. This may be useful for limited
 * situations in which an issuer-led policy cannot possibly be enforced at all
 * times, for example before a system has synchronized its system clock.
 */
CAPROCK_API extern char const * const CAPROCK_EXPIRY_POLICY_LOCAL;

/**
 * The wildcard can be provided as the subject or object of a
 * \ref caprock_claim.
 */
CAPROCK_API extern char const * const CAPROCK_CLAIMS_WILDCARD;


/**
 * The identifier size is provided so you can more easily check your inputs
 * up front; subject and object should be in the range described by this and
 * \ref CAPROCK_IDENTIFIER_MAX_SIZE.
 */
CAPROCK_API extern size_t const CAPROCK_IDENTIFIER_MIN_SIZE;
CAPROCK_API extern size_t const CAPROCK_IDENTIFIER_MAX_SIZE;


/**
 * A caprock_claim is an authorization tuple in the specifiation, and
 * consists of an (optional) subject who is to be authorized, a predicate
 * that describes the imparted privileges, and an optional object on which
 * the privileges are granted.
 *
 * The subject may be omitted, in which case global privileges on the object
 * are granted.
 *
 * The object may be omitted, in which case the predicate does not grant
 * privileges, but is a verified statement about the subject.
 *
 * The object may also be a \ref CAPROCK_CLAIMS_WILDCARD, in which case the
 * claim is made about all objects, future and past, that the issuer has
 * any authority over.
 *
 * Predicates may not be omitted. You also may not omit both the subject
 * and the object at the same time.
 */
typedef struct caprock_claim
{
  /** The subject pointer and \p subject_size describe the subject. */
  char const * subject;
  size_t       subject_size;

  /** Same as subject, but for the predicate. */
  char const * predicate;
  size_t       predicate_size;

  /** Same as subject, but for the object. */
  char const * object;
  size_t       object_size;
} caprock_claim;


/**
 * Token creation does not much care about some things, such as if a given
 * identifier is of a specific type. In order to enforce a protocol, suc
 *
 * Creating a token serializes the parameters as needed into the output buffer,
 * and adds a signature from the issuer key.
 */
typedef enum {
  CSA_AUTO = 0, /**< Let the library pick the best algorithm. */
  CSA_RAW  = 1, /**< Use the raw public key without any digest. This is only
                     suitable for Edwards curve keys. */
  CSA_SHA2 = 2, /**< Use SHA-2 to produce the signature. */
  CSA_SHA3 = 3, /**< Use SHA-3 to produce the signature. */
} caprock_signature_algorithm;


/**
 * Use this function to create and serialize a capability that grants some
 * privileges. The \ref caprock_token_create macro is provided for backwards
 * compatibility, but takes the same parameters.
 *
 * See \ref caprock_revocation_create for a function with the same parameters
 * that creates revocation capabilities.
 *
 * @param[out] buffer The output buffer to write the serialized data into.
 * @param[in,out] bufsize A pointer to the output buffer size. Initialize this to the
 *    maximum size of the output buffer before calling. Upon return, this should
 *    typically contain the amount of buffer space used.
 * @param[in] issuer A key pair for the issuer. Since the capability needs to be
 *    signed, a public and private key are both required.
 * @param[in] sequence_no A sequence number for the capability, so that grants and
 *    revocations may be ordered. It is only required that each issuer
 *    increments this by some value for each capability generated.
 * @param[in] from NULL-terminated time stamp in ISO-8601 format that specifies the
 *    start of the capability's validity.
 * @param[in] to Same format as \p from, but for the end of the validity period.
 * @param[in] expiry_policy One of \ref CAPROCK_EXPIRY_POLICY_ISSUER or
 *    \ref CAPROCK_EXPIRY_POLICY_LOCAL.
 * @param[in] claims A pointer to one or more \ref caprock_claim structures.
 * @param[in] claims_size The number of claims pointed to by \p claims.
 * @param[in] issuer_hash_length The hash length for the issuer identifier that
 *    is encoded into the capability. See \ref caprock_identifier_hash_length
 *    for possible values.
 * @param[in] sigalg The signature algorithm which should be used to sign the
 *    capability. Possible values are provided by \ref caprock_signature_algorithm.
 *
 * @retval CAPROCK_ERR_SUCCESS All good, the capability was created and signed.
 *    The \p bufsize parameter should now contain the amount of buffer used.
 * @retval CAPROCK_ERR_INVALID_VALUE Some parameter had an invalid value. Most
 *    often, this means NULL values have been passed to pointers. But it could
 *    als mean that the parameters do not make sense in combination with each
 *    other. Refer to the log output for details.
 * @retval CAPROCK_ERR_CODEC Creating the capability was successful, but the
 *    result could not be encoded into the buffer. Often, this means the buffer
 *    was too small.
 */
CAPROCK_API caprock_error_t
caprock_grant_create(
    void * buffer, size_t * bufsize,  // Output buffer
    caprock_key_pair const * issuer,  // Since we need to sign, a key pair.
    uint64_t sequence_no,             // Validity ensured by caller.
    char const * from,                // ISO-8601 as NULL-terminated string
    char const * to,                  // same
    char const * expiry_policy,       // One of the constants above.
    caprock_claim const * claims,     // Array of claims
    size_t claims_size,               // 1 for a single claim.
    caprock_identifier_hash_length issuer_hash_length,
    caprock_signature_algorithm sigalg
);

/// See \ref caprock_grant_create
#define caprock_token_create caprock_grant_create


/**
 * This function is largely equivalent to \ref caprock_grant_create, but
 * produces revocations. All parameters have the same semantics, as do
 * return values.
 */
CAPROCK_API caprock_error_t
caprock_revocation_create(
    void * buffer, size_t * bufsize,  // Output buffer
    caprock_key_pair const * issuer,  // Since we need to sign, a key pair.
    uint64_t sequence_no,             // Validity ensured by caller.
    char const * from,                // ISO-8601 as NULL-terminated string
    char const * to,                  // same
    char const * expiry_policy,       // One of the constants above.
    caprock_claim const * claims,     // Array of claims
    size_t claims_size,               // 1 for a single claim.
    caprock_identifier_hash_length issuer_hash_length,
    caprock_signature_algorithm sigalg
);


/**
 * Validating a token takes the token buffer and a public key as its inputs.
 * This function also validates revocations.
 *
 * @param[in] token The serialized capability of either grant or revocation
 *    type.
 * @param[in] token_size The size in octets of the token.
 * @param[in] now NULL-terminated time stamp in ISO-8601 format that specifies
 *    the current time. Validity is determined at this time point; you can pass
 *    any (valid) time here to determine validity at that time point.
 * @param[in] verifier A public key to verify the capability signature with.
 *
 * @retval CAPROCK_ERR_SUCCESS The capability is valid at the given point in
 *    time.
 * @retval CAPROCK_ERR_INVALID_VALUE Some parameter had an invalid value. Most
 *    often, this means NULL values have been passed to pointers. But it could
 *    als mean that the parameters do not make sense in combination with each
 *    other. Refer to the log output for details.
 * @retval CAPROCK_ERR_CODEC The capability could not be decoded.
 * @retval CAPROCK_ERR_VALIDATION Either the signature was not valid, or the
 *    capability's validity period does not include the passed timestamp. In
 *    either case, the capability needs to be treated as invalid.
 */
CAPROCK_API caprock_error_t
caprock_token_validate(
    void const * token, size_t token_size,  // Serialized token
    char const * now,                       // ISO-8601 current timestamp
    caprock_key const * verifier            // Public key
);


/**
 * An iterator function for a token chain. Each invocation should place a token
 * into the passed buffer, and return \ref CAPROCK_ERR_SUCCESS if it worked,
 * other errors if it did not. \ref CAPROCK_ERR_ITERATION_END indicates that
 * there is no more data the iterator can provide. If returned, no token should
 * be written to the buffer, i.e. it has one-past-end semantics.
 *
 * The baton can be used for carrying state for the iterator, and is passed to
 * every invocation.
 *
 * @param[out] buffer The buffer to place a serialized capability into.
 * @param[in, out] bufsize When called, this is the maximum size of the buffer.
 *    Upon return, an iterator should set this to the amount of buffer consumed
 *    for writing the capability.
 * @param[in] baton An opaque pointer useful for the iterator; see
 *    \ref caprock_claim_validate for how it is initalized.
 */
typedef caprock_error_t (*caprock_token_iterator)(
    void * buffer, size_t * bufsize,
    void * baton
);

/**
 *
 * Flags specify how to treat the iterator. The function cannot itself order
 * tokens, so if the iterator produces out-of-order tokens, those can be
 * ignored or the function can error. Similarly, gaps in the token chain may
 * be ignored, or produce errors.
 */
typedef enum caprock_validation_flags
{
  CIF_NONE                  = 0x00, /**< Just consume tokens in the order the
                                         iterator presents them. Gaps in the
                                         chain produce errors, and tokens with
                                         an invalid signature produce errors. */
  CIF_IGNORE_OUT_OF_ORDER   = 0x01, /**< Skip/ignore tokens that ate provided
                                         out of order. */
  CIF_IGNORE_GAPS           = 0x02, /**< Do not error when gaps in the token
                                         chain are detected. */
  CIF_IGNORE_FAILED_SIG     = 0x04, /**< Do not error when tokens with a failed
                                         signature are encountered. */

  CIF_ALL = CIF_IGNORE_OUT_OF_ORDER /**< Combine all of the above flags. */
    | CIF_IGNORE_GAPS
    | CIF_IGNORE_FAILED_SIG
    // TODO add flags as needed
} caprock_validation_flags;

/**
 * Given a token iterator for known tokens, verify that a requested action is
 * permitted. The requested action is in the the form of a caprock_claim, as
 * this structure includes a subject, predicate and object. We are therefore
 * essentially validating a claim, hence the function name.
 *
 * Since it's possible to validate multiple claims at the same time, we'll
 * permit this. Note, however, that on failures it is then not possible to
 * determine which claim failed to validate.
 *
 * Flags can influence the interpretation of iterator values, but also the
 * claim validation itself.
 *
 * @param [in] claims A pointer to one or more claims to validate.
 * @param [in] claims_size The number of claims to validate (size of \p claims).
 * @param now NULL-terminated time stamp in ISO-8601 format that specifies the
 *    current time point.
 * @param [out] buffer A buffer for temporarily holding tokens; scratch space.
 * @param [in] bufsize The size in octets of \p buffer.
 * @param [in] iterator An iterator function. This iterator produces tokens,
 *    ideally in order, based on which it is determined whether the \p claims
 *    are valid at the time point specified by \p now.
 * @param [in] iterator_baton An opaque pointer handed to the iterator.
 * @param [in] verifier A public key with which to validate tokens. Note that a
 *    single public key is sufficient by definition, as objects present in
 *    claims are "owned" (or managed) by an authority; verifier keys which
 *    are not representing this authority have no say over those objecs.
 * @param [in] flags Flags influencing how tokens should be processed; see
 *    \ref caprock_validation_flags for possible values.
 *
 * @retval CAPROCK_ERR_SUCCESS If all claims are valid.
 * @retval CAPROCK_ERR_INVALID_VALUE When parameters are invalid, such as
 *    passing NULL pointers for required values. Refer to the debug log for
 *    specifics.
 * @retval CAPROCK_ERR_VALIDATION If tokens are not presented in order and
 *    gap-free (but see \p flags for influencing this). Also returned if
 *    claims are not valid at the given time point.
 * @retval other Any other values that the iterator function may return.
 */
CAPROCK_API caprock_error_t
caprock_claim_validate(
    caprock_claim const * claims,     // Array of claims
    size_t claims_size,               // 1 for a single claim.
    char const * now,                 // ISO-8601 current timestamp
    void * buffer, size_t bufsize,    // Buffer for temporarily holding tokens
    caprock_token_iterator iterator,  // Tokens to process
    void * iterator_baton,            // Baton for the iterator; can be NULL
    caprock_key const * verifier,     // Public key
    int flags                         // Flags
);


/**
 * Extract an issuer identifier from a serialized token. This can then be used
 * to e.g. look up keys in a key store, or determine the scope of a claim.
 *
 * The identifier is an opaque, serialized identifier. The caller must allocate
 * sufficient memory for the identifier. At the time of writing, identifiers are
 * less than 100 Bytes in length. If insufficient buffer space is provided, the
 * return value is CAPROCK_ERR_OUT_OF_MEMORY. The issuer_buffer_size parameter
 * is set to the amount of buffer space used on success. On failure, the value
 * is the amount of buffer needed.
 *
 * If the input is not a valid token, CAPROCK_ERR_CODEC is returned. Invalid
 * inputs produce CAPROCK_ERR_INVALID_VALUE.
 *
 * @param [out] issuer_buffer Buffer to write issuer identifier into.
 * @param [in, out] issuer_buffer_size Size in octets of \p issuer_buffer.
 * @param [in] token_buffer Buffer that holds a token.
 * @param [in] token_buffer_size Size in octets of \p token_buffer.
 *
 * @retval CAPROCK_ERR_SUCCESS When the isuser was successfully extracted from
 *    the token.
 * @retval CAPROCK_ERR_INVALID_VALUE When parameters were invalid, such as e.g.
 *    NULL pointers for required parameters. Check the debug log for details.
 * @retval CAPROC_ERR_CODEC When the token could not be decoded.
 * @retval CAPROCK_ERR_VALIDATION When the token does not contain an issuer.
 * @retval CAPROCK_ERR_OUT_OF_MEMORY When the issuer field in the token is
 *    larger than \p issuer_buffer_size.
 */
CAPROCK_API caprock_error_t
caprock_issuer_from_token(
    void * issuer_buffer, size_t * issuer_buffer_size,
    void const * token_buffer, size_t token_buffer_size
);


/**
 * Create a claim's object identifier from an object name.
 *
 * @param [out] buffer The output buffer.
 * @param [in, out] bufsize The output buffer's maximum size before calling.
 *    After successful identifier creation, this contains the amount of data
 *    used.
 * @param [in] name The object name to create an identifier from.
 * @param [in] name_size The size of the object name. If 0 is given, the name must be
 *    NUL-terminated, and the size will be determined automatically.
 * @param [in] hash_length The hash length to use. Note that unlike when
 *    creating identifiers from keys, \ref CIH_ANY is *not* valid here, but
 *    explicit lengths from \ref caprock_identifier_hash_length are.
 *
 * @retval CAPROCK_ERR_SUCCESS On success; \p bufsize will contain the amount
 *    of data used.
 * @retval CAPROCK_ERR_INVALID_VALUE If any input parameters were missing or
 *    invalid.
 * @retval CAPROCK_ERR_CODEC If the identifier could not be encoded for some
 *    reason.
 */
CAPROCK_API caprock_error_t
caprock_create_object_id(void * buffer, size_t * bufsize,
    char const * name, size_t name_size,
    caprock_identifier_hash_length hash_length);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
