/*
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_ERROR_H
#define CAPROCK_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <caprock.h>

/**
 * Macros for building a table of known error codes and associated messages.
 **/
#if !defined(CAPROCK_START_ERRORS)
#define CAPROCK_START_ERRORS \
  typedef uint32_t caprock_error_t; \
  enum caprock_error_code {
#define CAPROCK_ERRDEF(name, code, desc) CAPROCK_ ## name = code,
#define CAPROCK_END_ERRORS \
    CAPROCK_ERROR_LAST, \
    CAPROCK_START_USER_RANGE = 1000 };
#define CAPROCK_ERROR_FUNCTIONS
#endif


/*****************************************************************************
 * Error definitions
 **/

/**
 * \typedef caprock_error_t
 * The error code type for the caprock library.
 */

/**
 * \enum caprock_error_code
 * All valid values for \ref caprock_error_t are defined in this enumeration.
 */


CAPROCK_START_ERRORS

CAPROCK_ERRDEF(ERR_SUCCESS,
    0,
    "No error")
    /**< No error */

CAPROCK_ERRDEF(ERR_UNEXPECTED,
    1,
    "Nobody expects the Spanish Inquisition!")
    /**< An unexpected error occurred. This should only be returned from code
     * branches that should never be reached, and indicates a bug in the
     * library. */

CAPROCK_ERRDEF(ERR_INVALID_VALUE,
    2,
    "An invalid parameter value was provided.")
    /**< Similar to POSIX EINVAL, an invalid parameter was provided. */

CAPROCK_ERRDEF(ERR_INVALID_KEY,
    10,
    "Invalid key data was passed to a function.")
    /**< Tried to create a cryptographic key from invalid input data. */

CAPROCK_ERRDEF(ERR_VALIDATION,
    11,
    "Could not validate token with the given key.")
    /**< Could not validate a token with a given key; this may mean that
     * the token was signed with a different key, or that the signature
     * is invalid. We cannot distinguish this further. */

CAPROCK_ERRDEF(ERR_CODEC,
    20,
    "Error en- or decoding value.")
    /**< An error occurred when encoding or deciding values. */

CAPROCK_ERRDEF(ERR_OUT_OF_MEMORY,
    21,
    "Out of memory or buffer size.")
    /**< Similar to POSIX ENOMEM, we reached a condition where there was
     * insufficient memory available. */

CAPROCK_ERRDEF(ERR_ITERATION_END,
    30,
    "End of iteration.")
    /**< An iterator had no more data, and iteration ended. */

// TODO add errors as required

CAPROCK_END_ERRORS


#if defined(CAPROCK_ERROR_FUNCTIONS)

/*****************************************************************************
 * Functions
 **/

/**
 * Return the error message associated with the given error code. Never returns
 * nullptr; if an unknown error code is given, an "unidentified error" string is
 * returned.
 *
 * @param [in] code The error code to query.
 * @return The message defined above to describe the error code.
 **/
CAPROCK_API char const * caprock_error_message(caprock_error_t code);

/**
 * Return a string representation of the given error code. Also never returns
 * nullptr, see \ref caprock_error_message above.
 *
 * @param [in] code The error code to query.
 * @return The name of the error constant (enum value) as a string.
 **/
CAPROCK_API char const * caprock_error_name(caprock_error_t code);


#endif // CAPROCK_ERROR_FUNCTIONS


/**
 * Undefine macros again
 **/
#undef CAPROCK_START_ERRORS
#undef CAPROCK_ERRDEF
#undef CAPROCK_END_ERRORS

#undef CAPROCK_ERROR_FUNCTIONS

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
