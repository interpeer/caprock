/*
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_KEYS_H
#define CAPROCK_KEYS_H

#include <caprock.h>

#include <caprock/codec.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Caprock uses cryptographic keys for signing and verification of tokens - but
 * the key data structure is an implementation detail. Suffice that keys can be
 * created from input buffers.
 *
 * Note that caprock internally uses the s3kr1t library for parsing keys, but
 * that library supports various serialization formats. Refer to it for details
 * on what is supported.
 */
typedef struct caprock_key caprock_key;

/**
 * Key pairs consist of a public and private key component.
 */
typedef struct caprock_key_pair
{
  caprock_key * pubkey;
  caprock_key * privkey;
} caprock_key_pair;


/**
 * Initialize an empty caprock_key.
 *
 * @param [out] key A (nulled) caprock_key pointer is expected here.
 */
CAPROCK_API void caprock_key_init(caprock_key ** key);

/**
 * Same as \ref caprock_key_init, but creates an empty-initialized
 * \ref caprock_key_pair instead.
 */
CAPROCK_API caprock_key_pair caprock_key_init_pair();


/**
 * @param [in] key (non-NULL) \ref caprock_key.
 * @return 1 if the key is valid/public/private, 0 otherwise.
 */
CAPROCK_API int caprock_key_is_valid(caprock_key const * key);

/** See \ref caprock_key_is_valid */
CAPROCK_API int caprock_key_is_public(caprock_key const * key);

/** See \ref caprock_key_is_valid */
CAPROCK_API int caprock_key_is_private(caprock_key const * key);

/**
 * Read a public key from a buffer.
 *
 * Note that this is a thin wrapper around
 * :cpp:function:`s3kr1t::key::read_pubkey`, so refer to that function
 * for information on the formats of input data that can be understood.
 *
 * @param [out] pubkey They \ref caprock_key structure to read the material into.
 * @param [in] buffer The buffer to read key material from.
 * @param [in] bufsize The size of the input \p buffer.
 * @param [in] password A NULL-terminated, optional password to decrypt the key
 *    material. Pass NULL if unused.
 *
 * @retval CAPROCK_ERR_SUCCESS On success.
 * @retval CAPROCK_ERR_INVALID_VALUE If the input could not be read or buffers
 *    provided were too small, etc.
 */
CAPROCK_API caprock_error_t
caprock_key_read_public(caprock_key * pubkey, void const * buffer, size_t bufsize,
    char const * password);

/**
 * Same as \ref caprock_key_read_public, but for private keys.
 */
CAPROCK_API caprock_error_t
caprock_key_read_private(caprock_key * privkey, void const * buffer, size_t bufsize,
    char const * password);

/**
 * Same as \ref caprock_key_read_public, but for key pairs.
 */
CAPROCK_API caprock_error_t
caprock_key_read_pair(caprock_key_pair * pair, void const * buffer, size_t bufsize,
    char const * password);


/**
 * Extract a public key from a key pair. Note that extracting the private key
 * is not typically necessary, so we will skip that function.
 *
 * @param [out] pubkey The structure into which to extract a public key; it will be
 *    allocated by this function.
 * @param [in] pair The key pair to extract the key from.
 *
 * @retval CAPROCK_ERR_SUCCESS On success.
 * @retval CAPROCK_ERR_INVALID_VALUE If any of the inputs are invalid.
 */
CAPROCK_API caprock_error_t
caprock_key_extract_public(caprock_key ** pubkey, caprock_key_pair const * pair);


/**
 * Create key(pair) identifier. This is either a raw public key or a hash thereof
 * written into the buffer; the digest algorithm chosen is specs conforming.
 *
 * Some keys may be smaller as raw public keys. Unless you need a specific
 * (e.g. smaller) identifier length, pass CIH_ANY to the serialization function;
 * it will pick the smaller of a raw key or default hash size.
 */
typedef enum {
  CIH_ANY = 0, /**< Let the library pick. */
  CIH_224 = 1, /**< Choose a 224 bit hash. */
  CIH_256 = 2, /**< Choose a 256 bit hash. */
  CIH_384 = 3, /**< Choose a 384 bit hash. */
  CIH_512 = 4, /**< Choose a 512 bit hash. */
} caprock_identifier_hash_length;


/**
 * Given a public key, create an identifier from it.
 *
 * @param [in] tag The \ref caprock_field_identifier tag to use; this can be
 *    one of \ref CFI_ISSUER_IDv1, \ref CFI_CLAIMv1_SUBJECT or
 *    \ref CFI_CLAIMv1_OBJECT.
 * @param [out] buffer The output buffer.
 * @param [in, out] bufsize The output buffer's size in octets. On success, the
 *    amount of buffer used.
 * @param [in] pubkey The public key to use as input.
 * @param [in] hash_length Any of the hash length values in
 *    \ref caprock_identifier_hash_length.
 *
 * @retval CAPROCK_ERR_SUCCESS On success.
 * @retval CAPROCK_ERR_INVALID_VALUE If any of the inputs are invalid.
 * @retval CAPROCK_ERR_CODEC If the identifier could not be serialized.
 */
CAPROCK_API caprock_error_t
caprock_key_identifier_public(
    caprock_field_identifier tag,
    void * buffer, size_t * bufsize,
    caprock_key const * pubkey,
    caprock_identifier_hash_length hash_length);

/**
 * Equivalent to \ref caprock_key_identifier_public, but takes a key pair as
 * the input argument.
 */
CAPROCK_API caprock_error_t
caprock_key_identifier_pair(
    caprock_field_identifier tag,
    void * buffer, size_t * bufsize,
    caprock_key_pair const * pair,
    caprock_identifier_hash_length hash_length);


/**
 * Free key.
 *
 * @param [in, out] key The key to free.
 */
CAPROCK_API void
caprock_key_free(caprock_key ** key);

/**
 * Free key pair. This frees all keys contained within.
 *
 * @param [in] pair The key pair to free.
 */
CAPROCK_API void
caprock_key_free_pair(caprock_key_pair * pair);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
