/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <stdio.h>
#include <stdlib.h>

#include <caprock/basics.h>

int main(int argc, char **argv)
{
  // The test is *very* simple; if we can call the token creation function,
  // then we're good. The results don't matter.
  char buf[20];
  size_t bufsize = sizeof(buf);

  caprock_token_create(&buf[0], &bufsize,
    NULL,
    1234,
    NULL,
    NULL,
    "foo",
    NULL,
    0,
    CIH_ANY, CSA_AUTO);
  fprintf(stdout, "Everything is OK (ignore any errors you might see)!\n");
  exit(0);
}
