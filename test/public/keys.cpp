/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <caprock/keys.h>

#include "test_keys.h"

namespace {

struct test_data
{
  // Has to be a reference so the keys don't get copied before they get
  // initialized. It also saves some space, nice.
  std::string const & privkey;
  char const *        name;
} tests[] = {
  { test_keys::ecdsa_private_key_p256, "EcDSA_p256", },
  { test_keys::ecdsa_private_key_p384, "EcDSA_p384", },
  { test_keys::ecdsa_private_key_p521, "EcDSA_p521", },
  { test_keys::rsa_other_private_key,  "RSA", },
  { test_keys::dsa_private_key,        "DSA", },
  { test_keys::ed25519_private_key,    "Ed25519", },
  { test_keys::ed448_private_key,      "Ed448", },
};

std::string test_name(testing::TestParamInfo<test_data> const & info)
{
  return info.param.name;
}

} // anonymous namespace

class Keys
  : public testing::TestWithParam<test_data>
{
};


TEST_P(Keys, simple_key_test)
{
  auto td = GetParam();

  // For more complex tests, see s3kr1t library. This just barely tests the API.
  caprock_key_pair kpair = caprock_key_init_pair();

  auto err = caprock_key_read_pair(
      &kpair,
      td.privkey.data(),
      td.privkey.size(),
      nullptr);

  ASSERT_EQ(CAPROCK_ERR_SUCCESS, err);

  ASSERT_EQ(1, caprock_key_is_valid(kpair.pubkey));
  ASSERT_EQ(1, caprock_key_is_public(kpair.pubkey));

  ASSERT_EQ(1, caprock_key_is_valid(kpair.privkey));
  ASSERT_EQ(1, caprock_key_is_private(kpair.privkey));

  char idbuf[1000];
  size_t bufsize = 5;
  ASSERT_EQ(CAPROCK_ERR_CODEC, caprock_key_identifier_pair(
        CFI_ISSUER_IDv1,
        idbuf, &bufsize,
        &kpair, CIH_ANY));
  ASSERT_EQ(0, bufsize);

  bufsize = sizeof(idbuf);
  ASSERT_EQ(CAPROCK_ERR_SUCCESS, caprock_key_identifier_pair(
        CFI_ISSUER_IDv1,
        idbuf, &bufsize,
        &kpair, CIH_ANY));
  ASSERT_GT(bufsize, 0);

  caprock_key * pubkey = nullptr;
  ASSERT_EQ(0, caprock_key_is_valid(pubkey));
  ASSERT_EQ(0, caprock_key_is_public(pubkey));
  ASSERT_EQ(0, caprock_key_is_private(pubkey));

  ASSERT_EQ(CAPROCK_ERR_SUCCESS, caprock_key_extract_public(
        &pubkey, &kpair));

  ASSERT_EQ(1, caprock_key_is_valid(pubkey));
  ASSERT_EQ(1, caprock_key_is_public(pubkey));
  ASSERT_EQ(0, caprock_key_is_private(pubkey));

  caprock_key_free(&pubkey);
  caprock_key_free_pair(&kpair);
}


INSTANTIATE_TEST_SUITE_P(misc, Keys, testing::ValuesIn(tests),
    test_name);
