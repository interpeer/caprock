/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <vector>

#include <caprock/basics.h>
#include <caprock/keys.h>

#include <liberate/string/hexencode.h>
#include <liberate/logging.h>
#include <liberate/random/unsafe_bits.h>

#include "test_keys.h"


namespace {

struct keypair_holder
{
  caprock_key_pair pair;

  inline keypair_holder()
    : pair{caprock_key_init_pair()}
  {
  }

  inline explicit keypair_holder(caprock_key_pair const & init)
    : pair{init}
  {
  }

  inline ~keypair_holder()
  {
    caprock_key_free_pair(&pair);
  }
};


struct key_holder
{
  caprock_key * key = nullptr;

  inline key_holder() = default;
  inline key_holder(key_holder const &) = default;
  inline key_holder(key_holder &&) = default;
  inline key_holder & operator=(key_holder const &) = default;

  // Takes ownership, because why not? No refcounting or some such,
  // this is pretty basic.
  inline explicit key_holder(caprock_key * _key)
    : key{_key}
  {
  }

  inline ~key_holder()
  {
    caprock_key_free(&key);
  }
};


inline keypair_holder
test_issuer_key_pair()
{
  caprock_key_pair kpair = caprock_key_init_pair();
  auto err = caprock_key_read_pair(
      &kpair,
      test_keys::ecdsa_private_key_p256.data(),
      test_keys::ecdsa_private_key_p256.size(),
      nullptr);
  EXPECT_EQ(CAPROCK_ERR_SUCCESS, err);
  return keypair_holder{kpair};
}


inline key_holder
test_unrelated_pubkey()
{
  caprock_key * pubkey = nullptr;
  caprock_key_init(&pubkey);
  auto err = caprock_key_read_public(
      pubkey,
      test_keys::rsa_public_key.data(),
      test_keys::rsa_public_key.size(),
      nullptr);
  EXPECT_EQ(CAPROCK_ERR_SUCCESS, err);
  return key_holder{pubkey};
}

static constexpr char const * TEST_FROM = "2022-04-14T14:18:18Z";
static constexpr char const * TEST_TO = "2022-04-14T14:18:18-01:00";
static constexpr char const * TEST_GOOD_TS = "2022-04-14T14:18:20Z";
static constexpr char const * TEST_BAD_TS = "2022-04-14T14:18:20-01:00";


static constexpr caprock_claim TEST_BAD_CLAIMS[] = {
  {
    nullptr, 14,
    "bar",   0,
    "baz",   0,
  },
  {
    "foo", 0,
    nullptr, 0,
    "baz", 0,
  },
  {
    "foo", 0,
    "bar", 0,
    nullptr, 0,
  },
};
static constexpr size_t TEST_BAD_CLAIMS_SIZE = sizeof(TEST_BAD_CLAIMS) / sizeof(caprock_claim);


static constexpr caprock_claim TEST_CLAIMS[] = {
  {
    "\x4c\x27\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 66,
    "bar", 0,
    "\x54\x27\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 66,
  },
  {
    "\x4c\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30,
    "foo", 0,
    "\x54\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 30,
  },
};
static constexpr size_t TEST_CLAIMS_SIZE = sizeof(TEST_CLAIMS) / sizeof(caprock_claim);

static constexpr char TEST_TOKEN[] = {
  '\x20', '\x01', '\x6c', '\x24', '\x00', '\x28', '\x27', '\xec', '\xd8', '\xa3', '\x99', '\x4a', '\xc1', '\xc4', '\xf1', '\xcc',
  '\x73', '\xca', '\xab', '\x7b', '\x2b', '\x05', '\x30', '\x5b', '\xd4', '\x57', '\x6f', '\x8f', '\xf6', '\x22', '\x63', '\x3d',
  '\x2b', '\xe5', '\x52', '\x9e', '\x8f', '\xd4', '\xb9', '\x81', '\x9e', '\x4c', '\x6d', '\x14', '\x3d', '\xce', '\xab', '\x96',
  '\x1a', '\xda', '\x01', '\xba', '\x37', '\xe6', '\x93', '\x53', '\x08', '\x9a', '\x00', '\x78', '\x5a', '\x1c', '\x12', '\xf4',
  '\xec', '\xe5', '\x3e', '\xe3', '\xdd', '\xe6', '\x98', '\x2c', '\xd2', '\x09', '\x30', '\x34', '\x40', '\x00', '\x00', '\x00',
  '\x62', '\x58', '\x2d', '\x34', '\x40', '\xff', '\xff', '\xff', '\xff', '\xff', '\xff', '\xff', '\xff', '\x44', '\x00', '\x48',
  '\x02', '\x4c', '\x27', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x50', '\x03', '\x62', '\x61', '\x72', '\x54', '\x27', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x4c', '\x03', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x50', '\x03', '\x66', '\x6f', '\x6f', '\x54', '\x03', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x67', '\x30', '\x45', '\x02', '\x20',
  '\x6c', '\xf9', '\x18', '\xcf', '\x0f', '\x3a', '\x75', '\x08', '\x88', '\x28', '\x21', '\x31', '\x63', '\x08', '\x28', '\x51',
  '\x93', '\x28', '\xc8', '\x41', '\x12', '\x53', '\x98', '\xb3', '\xb0', '\x6b', '\xd3', '\xa6', '\xd1', '\x36', '\x6f', '\x98',
  '\x02', '\x21', '\x00', '\x95', '\x73', '\xb8', '\x50', '\xe6', '\x36', '\x0d', '\x5f', '\xa7', '\x9c', '\x05', '\x58', '\xdd',
  '\x9d', '\x14', '\x18', '\x4c', '\xc3', '\xa0', '\xaa', '\x28', '\x7d', '\x67', '\x3e', '\xdf', '\x54', '\x6e', '\x62', '\x4b',
  '\x92', '\x68', '\xb1',
};


inline void
test_create_token(std::vector<char> & buf, keypair_holder const & kpair,
    char const * to = nullptr)
{
  buf.resize(1000);
  size_t bufsize = buf.size();

  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_grant_create(&buf[0], &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        to,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));

  liberate::string::canonical_hexdump hd;
  LIBLOG_DEBUG("Token dump: " << std::endl << hd(buf.data(), bufsize));

  // As a very rough estimate, the token must be larger than the claim
  // plus another identifier (ignore signature, and other metadata here)
  ASSERT_GT(bufsize, sizeof(TEST_CLAIMS) + 65);

  buf.resize(bufsize);
}


inline void
test_create_revocation(std::vector<char> & buf, keypair_holder const & kpair)
{
  buf.resize(1000);
  size_t bufsize = buf.size();

  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_revocation_create(&buf[0], &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        nullptr, // Infinite length permission
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));

  liberate::string::canonical_hexdump hd;
  LIBLOG_DEBUG("Token dump: " << std::endl << hd(buf.data(), bufsize));

  // As a very rough estimate, the token must be larger than the claim
  // plus another identifier (ignore signature, and other metadata here)
  ASSERT_GT(bufsize, sizeof(TEST_CLAIMS) + 65);

  buf.resize(bufsize);
}


inline std::vector<char>
test_default_token()
{
  return {TEST_TOKEN, TEST_TOKEN + sizeof(TEST_TOKEN)};
}


struct claim_deleter
{
  inline claim_deleter() = default;
  inline claim_deleter(claim_deleter const &) = default;
  // WTF, VC? inline claim_deleter(claim_deleter &) = default;
  inline claim_deleter(claim_deleter &&) = default;

  void operator()(caprock_claim * cl) const
  {
    if (!cl) {
      return;
    }

    delete [] cl->subject; cl->subject = nullptr;
    delete [] cl->predicate; cl->predicate = nullptr;
    delete [] cl->object; cl->object = nullptr;

    delete cl;
  };
};


template <typename rngT>
inline std::tuple<char *, size_t>
random_string(caprock_field_identifier id_tag, std::string const & prefix, rngT & rand)
{
  std::string num = "-" + std::to_string(rand.get());
  std::string tmp = "X" + prefix + "-";
  size_t remaining = (65 - tmp.size() - num.size());
  for (size_t i = 0 ; i < remaining ; ++i) {
    tmp += "X";
  }
  tmp += num;
  tmp[0] = id_tag;

  auto ptr = new char[tmp.size() + 1];
  strncpy(ptr, tmp.c_str(), tmp.size());
  ptr[tmp.size()] = '\0'; // Technically, this should not be required.
  return {ptr, tmp.size()};
}


template <typename rngT>
inline std::tuple<char *, size_t>
random_identifier(caprock_field_identifier id_tag, rngT & rand)
{
  // Figure out the type (and base don that, the size).
  caprock_field_identifier id_type;
  size_t size = 0;
  auto remainder = std::llabs(rand.get()) % 8;
  switch (remainder) {
    case 0:
      id_type = CFI_IDv1_NONE;
      size = 0;
      break;

    case 1:
      id_type = CFI_IDv1_WILDCARD;
      size = 0;
      break;

    case 2:
      id_type = CFI_IDv1_RAW_32;
      size = 32;
      break;

    case 3:
      id_type = CFI_IDv1_RAW_57;
      size = 57;
      break;

    case 4:
      id_type = CFI_IDv1_SHA3_28;
      size = 28;
      break;

    case 5:
      id_type = CFI_IDv1_SHA3_32;
      size = 32;
      break;

    case 6:
      id_type = CFI_IDv1_SHA3_48;
      size = 48;
      break;

    case 7:
      id_type = CFI_IDv1_SHA3_64;
      size = 64;
      break;

    default:
      std::cerr << "Test implementation error" << std::endl;
      return {nullptr, 0};
  }

  // Allocate size plus one
  auto ptr = new char[size + 2];
  ptr[0] = id_tag;
  ptr[1] = id_type;

  // The remaining memory is "random" in that it's uninitialized, but let's
  // just set it to random bits.
  for (size_t offset = 2 ; offset < size + 2 ; ++offset) {
    ptr[offset] = rand.get() % 256;
  }

  return {ptr, size + 2};
}



template <typename rngT>
inline std::unique_ptr<caprock_claim, claim_deleter>
create_random_claim(rngT & rand)
{
  auto ret = std::unique_ptr<caprock_claim, claim_deleter>{new caprock_claim{}};

  {
    auto [ptr, size] = random_identifier(CFI_CLAIMv1_SUBJECT, rand);
    ret->subject = ptr;
    ret->subject_size = size;
  }

  {
    auto [ptr, size] = random_string(CFI_CLAIMv1_PREDICATE, "predicate", rand);
    ret->predicate = ptr;
    ret->predicate_size = size;
  }

  {
    auto [ptr, size] = random_identifier(CFI_CLAIMv1_OBJECT, rand);
    ret->object = ptr;
    ret->object_size = size;
  }

  return ret;
}


inline uint64_t
contiguous_next(uint64_t prev)
{
  return prev + 1;
}


inline uint64_t
gapped_next(uint64_t prev)
{
  liberate::random::unsafe_bits<uint32_t> rand;

  // Gaps up to 20
  uint64_t ret = prev;
  do {
    ret = prev + (rand.get() % 20);
  } while (ret == prev || ret == std::numeric_limits<uint64_t>::max());
  return ret;
}


inline uint64_t
out_of_order_next(uint64_t prev)
{
  // Signed int means negative sequence mods possible
  liberate::random::unsafe_bits<int32_t> rand;

  // OOO up to 20
  // TODO: build config parameter
  uint64_t ret = prev;
  do {
    ret = prev + (std::llabs(rand.get()) % 20);
  } while (ret == prev || ret == std::numeric_limits<uint64_t>::max());
  return ret;
}


using next_func = std::function<uint64_t (uint64_t)>;

struct token_generator
{
  uint32_t max;
  uint32_t cur = 0;

  caprock_key_pair * issuer;

  caprock_claim * expected;

  next_func  next;

  uint64_t last_sequence_no = std::numeric_limits<uint64_t>::max();

  liberate::random::unsafe_bits<int32_t> rand;

  inline token_generator(uint32_t _max, caprock_claim * _expected, next_func _next,
      caprock_key_pair * _issuer
    )
    : max{0}
    , issuer{_issuer}
    , expected{_expected}
    , next{_next}
  {
    max = 1 + (rand.get() % _max);
  }

  inline caprock_error_t
  generate(void * buffer, size_t * bufsize)
  {
    uint32_t remaining = max - cur;
    if (remaining <= 0) {
      return CAPROCK_ERR_ITERATION_END;
    }

    // Randomly decide if we're to use the expected claim or some other.
    if (expected) {
      if (!(rand.get() % remaining)) {
        auto ret = generate_token(buffer, bufsize, expected, 1);
        expected = nullptr; // only use it once
        return ret;
      }
    }

    auto claim = create_random_claim(rand);
    return generate_token(buffer, bufsize, claim.get(), 1);
  }


  inline caprock_error_t
  generate_token(void * buffer, size_t * bufsize,
      caprock_claim const * claim, size_t claim_size)
  {
    uint64_t sequence_no = next(last_sequence_no);
    auto err = caprock_grant_create(buffer, bufsize,
        issuer,
        sequence_no,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        claim, claim_size,
        CIH_ANY, CSA_AUTO);
    if (CAPROCK_ERR_SUCCESS != err) {
      return err;
    }

    last_sequence_no = sequence_no;
    ++cur;
    return CAPROCK_ERR_SUCCESS;
  }
};


caprock_error_t
generator_trampoline(void * buffer, size_t * bufsize,
    void * baton)
{
  return static_cast<token_generator *>(baton)->generate(buffer, bufsize);
}


inline void
validate_claim(bool expect_success,
    next_func next, int flags)
{
  liberate::random::unsafe_bits<uint32_t> rand;

  // Use issuer keypair
  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // Create a test claim
  auto test_claim = create_random_claim(rand);

  // Create a generator with a maximum claim chain size, with or without the
  // test claim.
  token_generator claim_generator{
    200,
    expect_success ? test_claim.get() : nullptr,
    next,
    &kpair.pair
  };

  // Validate test claim. We'll provide a generous buffer here
  char buf[2000];
  auto err = caprock_claim_validate(
      test_claim.get(), 1,        // Test claim
      TEST_GOOD_TS, // FIXME 
      buf, sizeof(buf),           // Buffer
      &generator_trampoline, &claim_generator, // Generated tokens
      pubkey.key,                 // Verifier
      flags
  );

  if (expect_success) {
    ASSERT_EQ(CAPROCK_ERR_SUCCESS, err);
  }
  else {
    ASSERT_NE(CAPROCK_ERR_SUCCESS, err);
  }
}


} // anonymous namespace

TEST(Basics, create_token_null_buffer)
{
  auto kpair = test_issuer_key_pair();
  size_t bufsize = 123;

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(nullptr, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_null_buffer_size)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, nullptr,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_empty_buffer_size)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t tmp = 0;

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &tmp,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_null_keypair)
{
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        nullptr,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_empty_keypair)
{
  char buf[1];
  size_t bufsize = sizeof(buf);

  caprock_key_pair bad_kpair = caprock_key_init_pair();

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &bad_kpair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));

  caprock_key_free_pair(&bad_kpair);
}


TEST(Basics, create_token_empty_from)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        nullptr,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_bad_from)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        "badfrom",
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_bad_to)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        "badto",
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_null_policy)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        nullptr,
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_bad_policy)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        "fnargh",
        TEST_CLAIMS,
        TEST_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_bad_claim_subject)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        &TEST_BAD_CLAIMS[0],
        1,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_bad_claim_predicate)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        &TEST_BAD_CLAIMS[1],
        1,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_bad_claim_object)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        &TEST_BAD_CLAIMS[2],
        1,
        CIH_ANY, CSA_AUTO));
}



TEST(Basics, create_token_bad_claims)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_BAD_CLAIMS,
        TEST_BAD_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}



TEST(Basics, create_token_null_claims)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        nullptr,
        TEST_BAD_CLAIMS_SIZE,
        CIH_ANY, CSA_AUTO));
}


TEST(Basics, create_token_no_claims_size)
{
  auto kpair = test_issuer_key_pair();
  char buf[1];
  size_t bufsize = sizeof(buf);

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_grant_create(buf, &bufsize,
        &kpair.pair,
        1234,
        TEST_FROM,
        TEST_TO,
        CAPROCK_EXPIRY_POLICY_ISSUER,
        TEST_BAD_CLAIMS,
        0,
        CIH_ANY, CSA_AUTO));

}


TEST(Basics, create_token_success)
{
  auto kpair = test_issuer_key_pair();
  std::vector<char> buf;
  test_create_token(buf, kpair);
}


TEST(Basics, validate_token_bad_type)
{
  auto kpair = test_issuer_key_pair();
  auto token = test_default_token();

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // The token type is at offset 1 and should be never be 42
  token[1] = 42;

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_CODEC,
      caprock_token_validate(token.data(), token.size(),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, validate_token_bad_scope_tag)
{
  auto kpair = test_issuer_key_pair();
  auto token = test_default_token();

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // The scope tag should be at offset 71
  token[71] = 42;

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_CODEC,
      caprock_token_validate(token.data(), token.size(),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, validate_token_bad_expiry_policy)
{
  auto kpair = test_issuer_key_pair();
  auto token = test_default_token();

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // The expiry policy should be at offset 95
  token[95] = 42;

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_CODEC,
      caprock_token_validate(token.data(), token.size(),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, validate_token_bad_input)
{
  auto kpair = test_issuer_key_pair();

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  char token[421]; // Do *not* initialize

  ASSERT_EQ(CAPROCK_ERR_CODEC,
      caprock_token_validate(token, sizeof(token),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, validate_token_no_timestamp)
{
  auto kpair = test_issuer_key_pair();
  auto token = test_default_token();

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_token_validate(token.data(), token.size(),
        nullptr,
        pubkey.key));
}


TEST(Basics, validate_token_bad_timestamp)
{
  auto kpair = test_issuer_key_pair();
  std::vector<char> buf;
  test_create_token(buf, kpair, TEST_TO);

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_VALIDATION,
      caprock_token_validate(buf.data(), buf.size(),
        TEST_BAD_TS,
        pubkey.key));
}


TEST(Basics, validate_token_bad_key)
{
  auto unrelated = test_unrelated_pubkey();
  auto token = test_default_token();

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_VALIDATION,
      caprock_token_validate(token.data(), token.size(),
        TEST_GOOD_TS,
        unrelated.key));
}


TEST(Basics, validate_token_success)
{
  auto kpair = test_issuer_key_pair();
  auto token = test_default_token();

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Token dump: " << std::endl << hd(token.data(), token.size()));

  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_token_validate(token.data(), token.size(),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, create_and_validate_token_success)
{
  // This is different from the above,because the signature gets generated and
  // is not hardcoded.

  auto kpair = test_issuer_key_pair();
  std::vector<char> buf;
  test_create_token(buf, kpair);

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_token_validate(buf.data(), buf.size(),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, create_and_validate_revocation)
{
  auto kpair = test_issuer_key_pair();
  std::vector<char> buf;
  test_create_revocation(buf, kpair);

  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));

  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_token_validate(buf.data(), buf.size(),
        TEST_GOOD_TS,
        pubkey.key));
}


TEST(Basics, validate_claim_bad_claims)
{
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        nullptr,
        1234,
        TEST_GOOD_TS,
        buf, sizeof(buf),
        &generator_trampoline,
        nullptr,
        pubkey.key,
        CIF_NONE));
}


TEST(Basics, validate_claim_bad_claim_size)
{
  caprock_claim claim;
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        0,
        TEST_GOOD_TS,
        buf, sizeof(buf),
        &generator_trampoline,
        nullptr,
        pubkey.key,
        CIF_NONE));
}


TEST(Basics, validate_claim_bad_buffer)
{
  caprock_claim claim;
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        1,
        TEST_GOOD_TS,
        nullptr, sizeof(buf),
        &generator_trampoline,
        nullptr,
        pubkey.key,
        CIF_NONE));
}


TEST(Basics, validate_claim_bad_buffer_size)
{
  caprock_claim claim;
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        1,
        TEST_GOOD_TS,
        buf, 0,
        &generator_trampoline,
        nullptr,
        pubkey.key,
        CIF_NONE));
}


TEST(Basics, validate_claim_bad_iterator)
{
  caprock_claim claim;
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        1,
        TEST_GOOD_TS,
        buf, sizeof(buf),
        nullptr,
        nullptr,
        pubkey.key,
        CIF_NONE));
}


TEST(Basics, validate_claim_bad_key)
{
  caprock_claim claim;
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        1,
        TEST_GOOD_TS,
        buf, sizeof(buf),
        &generator_trampoline,
        nullptr,
        nullptr,
        CIF_NONE));
}


TEST(Basics, validate_claim_bad_flags)
{
  caprock_claim claim;
  char buf[12];

  auto kpair = test_issuer_key_pair();
  key_holder pubkey;
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_key_extract_public(&pubkey.key, &kpair.pair));


  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        1,
        TEST_GOOD_TS,
        buf, sizeof(buf),
        &generator_trampoline,
        nullptr,
        pubkey.key,
        42));

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_claim_validate(
        &claim,
        1,
        TEST_GOOD_TS,
        buf, sizeof(buf),
        &generator_trampoline,
        nullptr,
        pubkey.key,
        -42));
}


TEST(Basics, validate_claim_contiguous_failure)
{
  validate_claim(
      false,
      &contiguous_next,
      CIF_NONE
  );
}


TEST(Basics, validate_claim_contiguous_success)
{
  validate_claim(
      true,
      &contiguous_next,
      CIF_NONE
  );
}


TEST(Basics, validate_claim_gaps_failure)
{
  validate_claim(
      false,
      &gapped_next,
      CIF_NONE
  );
}


TEST(Basics, validate_claim_gaps_success)
{
  validate_claim(
      true,
      &gapped_next,
      CIF_IGNORE_GAPS
  );
}


TEST(Basics, validate_claim_out_of_order_failure)
{
  validate_claim(
      false,
      &out_of_order_next,
      CIF_NONE
  );
}


TEST(Basics, validate_claim_out_of_order_success)
{
  validate_claim(
      true,
      &out_of_order_next,
      CIF_IGNORE_OUT_OF_ORDER | CIF_IGNORE_GAPS
  );
}


TEST(Basics, extract_issuer_bad_inputs)
{
  char buf[100];

  // Bad inputs
  size_t issuer_size = sizeof(buf);
  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_issuer_from_token(nullptr, nullptr,
        TEST_TOKEN, sizeof(TEST_TOKEN)));

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_issuer_from_token(nullptr, &issuer_size,
        TEST_TOKEN, sizeof(TEST_TOKEN)));

  issuer_size = 0;
  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_issuer_from_token(buf, &issuer_size,
        TEST_TOKEN, sizeof(TEST_TOKEN)));

  issuer_size = sizeof(buf);
  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_issuer_from_token(buf, &issuer_size,
        nullptr, sizeof(TEST_TOKEN)));

  ASSERT_EQ(CAPROCK_ERR_INVALID_VALUE,
      caprock_issuer_from_token(buf, &issuer_size,
        TEST_TOKEN, 0));

  // Bad token
  ASSERT_EQ(CAPROCK_ERR_CODEC,
      caprock_issuer_from_token(buf, &issuer_size,
        "Hello, world!", 13));

  // To little issuer buffer
  issuer_size = 10;
  ASSERT_EQ(CAPROCK_ERR_OUT_OF_MEMORY,
      caprock_issuer_from_token(buf, &issuer_size,
        TEST_TOKEN, sizeof(TEST_TOKEN)));
  ASSERT_GT(issuer_size, 10);
}


TEST(Basics, extract_issuer_ok)
{
  char buf[100];

  size_t issuer_size = sizeof(buf);
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_issuer_from_token(buf, &issuer_size,
        TEST_TOKEN, sizeof(TEST_TOKEN)));
  ASSERT_EQ(64, issuer_size);
}


TEST(Basics, object_identifier)
{
  char buf[100];

  size_t bufsize = sizeof(buf);
  ASSERT_EQ(CAPROCK_ERR_SUCCESS,
      caprock_create_object_id(buf, &bufsize,
        "this is a very long name that definitely must be hashed", 0,
        CIH_256));
  ASSERT_EQ(34, bufsize);
}


/*****************************************************************************
 * Create tokens with various test keys
 */

namespace {

struct test_data
{
  // Has to be a reference so the keys don't get copied before they get
  // initialized. It also saves some space, nice.
  std::string const & privkey;
  char const *        name;
} tests[] = {
  { test_keys::ecdsa_private_key_p256, "EcDSA_p256", },
  { test_keys::ecdsa_private_key_p384, "EcDSA_p384", },
  { test_keys::ecdsa_private_key_p521, "EcDSA_p521", },
  { test_keys::rsa_other_private_key,  "RSA", },
  { test_keys::dsa_private_key,        "DSA", },
  { test_keys::ed25519_private_key,    "Ed25519", },
  { test_keys::ed448_private_key,      "Ed448", },
};

std::string test_name(testing::TestParamInfo<test_data> const & info)
{
  return info.param.name;
}

} // anonymous namespace

class BasicsKeys
  : public testing::TestWithParam<test_data>
{
};


TEST_P(BasicsKeys, create_token_success)
{
  auto td = GetParam();

  caprock_key_pair pair = caprock_key_init_pair();
  auto err = caprock_key_read_pair(
      &pair,
      td.privkey.data(),
      td.privkey.size(),
      nullptr);
  EXPECT_EQ(CAPROCK_ERR_SUCCESS, err);
  auto kpair = keypair_holder{pair};

  std::vector<char> buf;
  test_create_token(buf, kpair);
}

INSTANTIATE_TEST_SUITE_P(misc, BasicsKeys, testing::ValuesIn(tests),
    test_name);
