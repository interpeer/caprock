/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <vector>

#include "codec.h"

#include <liberate/string/hexencode.h>

namespace {

inline void
test_identifier(
    caprock_field_identifier tag,
    caprock_field_identifier type, size_t size)
{
  char buf[100];

  caprock::codec::v1::identifier_t id;
  id.id_tag = tag;
  id.id_type = type;
  id.id_buffer.resize(size);
  id.id_buffer[0] = 0xde;
  id.id_buffer[1] = 0xad;
  id.id_buffer[2] = 0xd0;
  id.id_buffer[3] = 0x0d;

//  liberate::string::canonical_hexdump hd;
//  std::cerr << "ID dump:" << std::endl << hd(id.id_buffer.data(), id.id_buffer.size())
//    << std::endl;

  // Too small
  auto used = caprock::codec::v1::encode_identifier(buf, 2, id);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_identifier(buf, sizeof(buf), id);
  ASSERT_EQ(size + 2, used);

//  std::cerr << "Serialized dump:" << std::endl << hd(buf, used) << std::endl;

  // Decode - too small
  caprock::codec::v1::identifier_t id2;
  used = caprock::codec::v1::decode_identifier(id2, buf, 2);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_identifier(id2, buf, sizeof(buf));
  ASSERT_EQ(size + 2, used);

//  std::cerr << "ID2 dump:" << std::endl << hd(id2.id_buffer.data(), id2.id_buffer.size())
//    << std::endl;

  ASSERT_EQ(tag, id2.id_tag);
  ASSERT_EQ(type, id2.id_type);
  ASSERT_EQ(size, id2.id_buffer.size());
  ASSERT_EQ(0xde, id2.id_buffer[0]);
  ASSERT_EQ(0xad, id2.id_buffer[1]);
  ASSERT_EQ(0xd0, id2.id_buffer[2]);
  ASSERT_EQ(0x0d, id2.id_buffer[3]);
}

} // anonymous namespace



TEST(Codec, token_header)
{
  char buf[100];

  // Too small
  auto used = caprock::codec::v1::encode_token_header(buf, 2, 42);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_token_header(buf, sizeof(buf), 42);
  ASSERT_EQ(3, used);

  // Decode - too small
  size_t token_size = 0; 
  used = caprock::codec::v1::decode_token_header(token_size, buf, 2);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_token_header(token_size, buf, sizeof(buf));
  ASSERT_EQ(3, used);

  ASSERT_EQ(42, token_size);
}



TEST(Codec, token_type_grant)
{
  char buf[100];

  // Too small
  auto used = caprock::codec::v1::encode_token_type(buf, 1, CTT_GRANT);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_token_type(buf, sizeof(buf), CTT_GRANT);
  ASSERT_EQ(2, used);

  // Decode - too small
  caprock_token_type type;
  used = caprock::codec::v1::decode_token_type(type, buf, 1);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_token_type(type, buf, sizeof(buf));
  ASSERT_EQ(2, used);

  ASSERT_EQ(CTT_GRANT, type);
}



TEST(Codec, token_type_revoke)
{
  char buf[100];

  // Too small
  auto used = caprock::codec::v1::encode_token_type(buf, 1, CTT_REVOKE);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_token_type(buf, sizeof(buf), CTT_REVOKE);
  ASSERT_EQ(2, used);

  // Decode - too small
  caprock_token_type type;
  used = caprock::codec::v1::decode_token_type(type, buf, 1);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_token_type(type, buf, sizeof(buf));
  ASSERT_EQ(2, used);

  ASSERT_EQ(CTT_REVOKE, type);
}



TEST(Codec, identifier_none)
{
  char buf[100];

  caprock::codec::v1::identifier_t id;
  id.id_tag = CFI_ISSUER_IDv1;
  id.id_type = CFI_IDv1_NONE;

  // Too small
  auto used = caprock::codec::v1::encode_identifier(buf, 0, id);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_identifier(buf, sizeof(buf), id);
  ASSERT_EQ(2, used);

  // Decode - too small
  caprock::codec::v1::identifier_t id2;
  used = caprock::codec::v1::decode_identifier(id2, buf, 0);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_identifier(id2, buf, sizeof(buf));
  ASSERT_EQ(2, used);

  ASSERT_EQ(CFI_ISSUER_IDv1, id2.id_tag);
  ASSERT_EQ(CFI_IDv1_NONE, id2.id_type);
  ASSERT_EQ(0, id2.id_buffer.size());
}



TEST(Codec, identifier_wildcard)
{
  char buf[100];

  caprock::codec::v1::identifier_t id;
  id.id_tag = CFI_CLAIMv1_SUBJECT;
  id.id_type = CFI_IDv1_WILDCARD;

  // Too small
  auto used = caprock::codec::v1::encode_identifier(buf, 0, id);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_identifier(buf, sizeof(buf), id);
  ASSERT_EQ(2, used);

  // Decode - too small
  caprock::codec::v1::identifier_t id2;
  used = caprock::codec::v1::decode_identifier(id2, buf, 0);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_identifier(id2, buf, sizeof(buf));
  ASSERT_EQ(2, used);

  ASSERT_EQ(CFI_CLAIMv1_SUBJECT, id2.id_tag);
  ASSERT_EQ(CFI_IDv1_WILDCARD, id2.id_type);
  ASSERT_EQ(0, id2.id_buffer.size());
}



TEST(Codec, identifier_raw_32)
{
  test_identifier(CFI_ISSUER_IDv1, CFI_IDv1_RAW_32, 32);
}


TEST(Codec, identifier_raw_57)
{
  test_identifier(CFI_CLAIMv1_SUBJECT, CFI_IDv1_RAW_57, 57);
}


TEST(Codec, identifier_sha3_28)
{
  test_identifier(CFI_CLAIMv1_OBJECT, CFI_IDv1_SHA3_28, 28);
}


TEST(Codec, identifier_sha3_32)
{
  test_identifier(CFI_ISSUER_IDv1, CFI_IDv1_SHA3_32, 32);
}


TEST(Codec, identifier_sha3_48)
{
  test_identifier(CFI_CLAIMv1_SUBJECT, CFI_IDv1_SHA3_48, 48);
}


TEST(Codec, identifier_sha3_64)
{
  test_identifier(CFI_CLAIMv1_OBJECT, CFI_IDv1_SHA3_64, 64);
}



TEST(Codec, sequence_number)
{
  char buf[100];

  // Too small
  auto used = caprock::codec::v1::encode_sequence_number(buf, 2, 0xf007b411);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_sequence_number(buf, sizeof(buf), 0xf007b411);
  ASSERT_EQ(6, used);

  // Decode - too small
  uint64_t seq_no = 0; 
  used = caprock::codec::v1::decode_sequence_number(seq_no, buf, 2);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_sequence_number(seq_no, buf, sizeof(buf));
  ASSERT_EQ(6, used);

  ASSERT_EQ(0xf007b411, seq_no);
}


TEST(Codec, scope_with_to)
{
  char buf[100];

  using namespace std::chrono_literals;

  caprock::codec::v1::scope_t scope;
  auto now = std::chrono::round<std::chrono::seconds>(
    std::chrono::time_point_cast<caprock::util::time_point<>::duration>(std::chrono::system_clock::now())
  );
  scope.from = now;
  scope.to = scope.from + 20s;
  scope.expiry_policy = CEP_ISSUER;

  // Too small
  auto used = caprock::codec::v1::encode_scope(buf, 3, scope);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_scope(buf, sizeof(buf), scope);
  ASSERT_EQ(caprock::codec::v1::SCOPE_SIZE, used);

  // Decode - too small
  caprock::codec::v1::scope_t scope2;
  used = caprock::codec::v1::decode_scope(scope2, buf, 3);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_scope(scope2, buf, sizeof(buf));
  ASSERT_EQ(caprock::codec::v1::SCOPE_SIZE, used);

  // Check results
  ASSERT_EQ(now, scope2.from);
  ASSERT_TRUE(scope2.to.has_value());
  ASSERT_EQ(now + 20s, scope2.to.value());
  ASSERT_EQ(CEP_ISSUER, scope2.expiry_policy);
}



TEST(Codec, scope_without_to)
{
  char buf[100];

  using namespace std::chrono_literals;

  caprock::codec::v1::scope_t scope;
  auto now = std::chrono::round<std::chrono::seconds>(
    std::chrono::time_point_cast<caprock::util::time_point<>::duration>(std::chrono::system_clock::now())
  );
  scope.from = now;
  scope.expiry_policy = CEP_ISSUER;

  // Too small
  auto used = caprock::codec::v1::encode_scope(buf, 3, scope);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_scope(buf, sizeof(buf), scope);
  ASSERT_EQ(caprock::codec::v1::SCOPE_SIZE, used);

  // Decode - too small
  caprock::codec::v1::scope_t scope2;
  used = caprock::codec::v1::decode_scope(scope2, buf, 3);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_scope(scope2, buf, sizeof(buf));
  ASSERT_EQ(caprock::codec::v1::SCOPE_SIZE, used);

  // Check results
  ASSERT_EQ(now, scope2.from);
  ASSERT_FALSE(scope2.to.has_value());
  ASSERT_EQ(CEP_ISSUER, scope2.expiry_policy);
}



TEST(Codec, claims_with_object)
{
  char buf[100];

  caprock::codec::v1::claim_t claim;

  claim.subject.id_type = CFI_IDv1_RAW_32;
  claim.subject.id_buffer.resize(32);
  claim.subject.id_buffer[0] = 0xf0;
  claim.subject.id_buffer[1] = 0x07;
  claim.subject.id_buffer[30] = 0xfa;
  claim.subject.id_buffer[31] = 0x11;

  claim.predicate = "foo.bar";

  caprock::codec::v1::identifier_t obj_id;
  obj_id.id_type = CFI_IDv1_SHA3_48;
  obj_id.id_buffer.resize(48);
  obj_id.id_buffer[0] = 0xde;
  obj_id.id_buffer[1] = 0xad;
  obj_id.id_buffer[46] = 0xd0;
  obj_id.id_buffer[47] = 0x0d;
  claim.object = obj_id;

  claim.update_hash(); // XXX: not really required here

  std::vector<caprock::codec::v1::claim_t> claims;
  claims.push_back(claim);

  // Too small
  auto used = caprock::codec::v1::encode_claims(buf, 3, claims);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_claims(buf, sizeof(buf), claims);
  ASSERT_GT(used, 30);

  // liberate::string::canonical_hexdump hd;
  // std::cerr << "Dump:" << std::endl << hd(buf, used) << std::endl;

  // Decode - too small
  claims.clear();
  used = caprock::codec::v1::decode_claims(claims, buf, 3);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_claims(claims, buf, sizeof(buf));
  ASSERT_GT(used, 30);

  // Check results
  ASSERT_EQ(1, claims.size());

  ASSERT_EQ(CFI_CLAIMv1_SUBJECT, claims[0].subject.id_tag);
  ASSERT_EQ(CFI_IDv1_RAW_32, claims[0].subject.id_type);
  ASSERT_EQ(claim.subject.id_buffer, claims[0].subject.id_buffer);

  ASSERT_EQ(claim.predicate, claims[0].predicate);

  ASSERT_TRUE(claims[0].object.has_value());
  ASSERT_EQ(CFI_CLAIMv1_OBJECT, claims[0].object.value().id_tag);
  ASSERT_EQ(CFI_IDv1_SHA3_48, claims[0].object.value().id_type);
  ASSERT_EQ(claim.object.value().id_buffer, claims[0].object.value().id_buffer);
}



TEST(Codec, claims_without_object)
{
  char buf[100];

  caprock::codec::v1::claim_t claim;

  claim.subject.id_type = CFI_IDv1_RAW_32;
  claim.subject.id_buffer.resize(32);
  claim.subject.id_buffer[0] = 0xf0;
  claim.subject.id_buffer[1] = 0x07;
  claim.subject.id_buffer[30] = 0xfa;
  claim.subject.id_buffer[31] = 0x11;

  claim.predicate = "foo.bar";

  claim.update_hash(); // XXX: not really required here

  std::vector<caprock::codec::v1::claim_t> claims;
  claims.push_back(claim);

  // Too small
  auto used = caprock::codec::v1::encode_claims(buf, 3, claims);
  ASSERT_EQ(0, used);

  // Works
  used = caprock::codec::v1::encode_claims(buf, sizeof(buf), claims);
  ASSERT_GT(used, 30);

  // liberate::string::canonical_hexdump hd;
  // std::cerr << "Dump:" << std::endl << hd(buf, used) << std::endl;

  // Decode - too small
  claims.clear();
  used = caprock::codec::v1::decode_claims(claims, buf, 3);
  ASSERT_EQ(0, used);

  used = caprock::codec::v1::decode_claims(claims, buf, sizeof(buf));
  ASSERT_GT(used, 30);

  // Check results
  ASSERT_EQ(1, claims.size());

  ASSERT_EQ(CFI_CLAIMv1_SUBJECT, claims[0].subject.id_tag);
  ASSERT_EQ(CFI_IDv1_RAW_32, claims[0].subject.id_type);
  ASSERT_EQ(claim.subject.id_buffer, claims[0].subject.id_buffer);

  ASSERT_EQ(claim.predicate, claims[0].predicate);

  ASSERT_TRUE(claims[0].object.has_value());
  ASSERT_EQ(CFI_CLAIMv1_OBJECT, claims[0].object.value().id_tag);
  ASSERT_EQ(CFI_IDv1_NONE, claims[0].object.value().id_type);
}


TEST(Codec, token)
{
  char buf[300];

  caprock::codec::v1::token_t token;

  token.type = CTT_GRANT;

  token.issuer.id_tag = CFI_ISSUER_IDv1;
  token.issuer.id_type = CFI_IDv1_RAW_32;
  token.issuer.id_buffer.resize(32);
  std::memset(&(token.issuer.id_buffer[0]), '\xaa', token.issuer.id_buffer.size());

  token.sequence_no = 42;

  using namespace std::chrono_literals;
  auto now = std::chrono::round<std::chrono::seconds>(
    std::chrono::time_point_cast<caprock::util::time_point<>::duration>(std::chrono::system_clock::now())
  );
  token.scope.from = now;
  token.scope.to = token.scope.from + 20s;
  token.scope.expiry_policy = CEP_ISSUER;

  {
    caprock::codec::v1::claim_t claim;
    claim.subject.id_tag = CFI_CLAIMv1_SUBJECT;
    claim.subject.id_type = CFI_IDv1_RAW_32;
    claim.subject.id_buffer.resize(32);
    std::memset(&(claim.subject.id_buffer[0]), '\xbb', claim.subject.id_buffer.size());

    claim.predicate = "foo.bar";

    caprock::codec::v1::identifier_t id;
    id.id_tag = CFI_CLAIMv1_OBJECT;
    id.id_type = CFI_IDv1_WILDCARD;
    claim.object = id;

    token.claims.push_back(claim);
  }

  {
    caprock::codec::v1::claim_t claim;
    claim.subject.id_tag = CFI_CLAIMv1_SUBJECT;
    claim.subject.id_type = CFI_IDv1_RAW_32;
    claim.subject.id_buffer.resize(32);
    std::memset(&(claim.subject.id_buffer[0]), '\xcc', claim.subject.id_buffer.size());

    claim.predicate = "baz.quux";

    caprock::codec::v1::identifier_t id;
    id.id_tag = CFI_CLAIMv1_OBJECT;
    id.id_type = CFI_IDv1_SHA3_64;
    id.id_buffer.resize(64);
    std::memset(&(id.id_buffer[0]), '\xdd', id.id_buffer.size());
    claim.object = id;

    token.claims.push_back(claim);
  }

  token.sig_type = CFI_SIGv1_SHA2_28;
  token.sig_buffer.resize(28);

  token.update_hash();

  // Too small
  {
    auto [used, payload] = caprock::codec::v1::encode_token_payload(buf, 3, token);
    ASSERT_EQ(0, used);
  }

  // Works
  {
    auto [used, payload] = caprock::codec::v1::encode_token_payload(buf, sizeof(buf), token);
    ASSERT_GT(used, 30);
  }

  // liberate::string::canonical_hexdump hd;
  // std::cerr << "Dump:" << std::endl << hd(buf, used) << std::endl;

  // Decode - too small
  caprock::codec::v1::token_t token2;
  {
    auto [used, _] = caprock::codec::v1::decode_token(token2, buf, 3);
    ASSERT_EQ(0, used);
  }

  {
    auto [used, _] = caprock::codec::v1::decode_token(token2, buf, sizeof(buf));
    ASSERT_GT(used, 30);
  }

  // Check results
  ASSERT_EQ(token, token2);
}
