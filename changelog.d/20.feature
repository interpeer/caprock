Support various key types in the specs, and resulting variable length
identifiers (RSA, DSA, EDDSA and Ed25519/Ed448 keys).
