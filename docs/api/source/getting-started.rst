===============
Getting Started
===============

The CAProck library is implemented in portable C++, but offers a C-only API.
The C API was chosen in order to most easily integrate the library in other
projects and languages, whereas the implementation language was chosesn to
provide better memory safety and similar conveniences during implementation.

As a user of the library, you need only be concerned with the C API.

You would usually use :ref:`Meson <using-meson>` when contributing to the
library itself; that build system also provides some means for dependency
management, which may help you in your project. If you do not wish to use
Meson for whichever reason, try the approach with :ref:`Conan <using-conan>`
below.

.. _using-meson:

Using Meson
===========

.. highlight:: shell
   :linenothreshold: 5

CAProck is built using `meson <https://mesonbuild.com>`_, because this
provides cross-platform builds that are relatively easily managed. Meson
is written in `Python <https://python.org>`_, as is Sphinx, the tool with
which this documentation is generated.

Install Tooling
---------------

Because there is so much Python based tooling in this project, it seemed
best to use Pythonic means for managing the tool dependencies, so that is
what we're doing.

#. Install Python version 3 or above. Most Linux systems have this already
   installed; BSD systems provide it. Use your system's best installation
   method for this.

   Python comes with a command ``pip`` (sometimes ``pip3``), with which you
   install packages.
#. Install the ``pipenv`` package::

   $ pip3 install -U pipenv
#. Use pipenv to install the development tools::

   $ pipenv install
#. Activate the pipenv virtual environment::

   $ pipenv shell

At this point the shell you're running has the development tools installed
locally.

Build with Meson
----------------

Meson expects a ``build`` folder separate from the source folder. Typically
you might create this within the repository working directory; this is what
we're doing here in the example.

.. code-block:: shell

   $ cd /path/to/repo
   $ mkdir build
   $ cd build

From there, you need to run Meson.

.. code-block:: shell

   $ meson setup ..
   $ ninja

For more information on Meson usage, see
`the meson documentation <https://mesonbuild.com>`_. You may need to install
`ninja <https://ninja-build.org/>`_ separately.

Documentation
-------------

For generating documentation, we need `Sphinx <https://www.sphinx-doc.org>`_,
and some extensions. These are installed by passing an extra flag to the pipenv
installation step above:

.. code-block:: shell

   $ pipenv install --dev

Note that you will need to separately install `Doxygen <https://www.doxygen.nl>`_,
which is typically provided by your system's package manager.

To build the documentation, do the following:

.. code-block:: shell

   $ cd /path/to/repo/docs/api
   $ make html

.. _using-conan:

Using Conan
===========

We provide (semi-)regular builds of the library using
`conan <https://conan.io/>`_. These are hosted on codeberg, where our code is
also hosted. The site also `includes instructions on installing
caprock via conan <https://codeberg.org/interpeer/-/packages/conan/caprock/>`_.
