===================================
Welcome to CAProck's documentation!
===================================

This library provides means for creating and serializing cryptographic
capabilities, that can then be transmitted over a network or stored, etc.
This allows for building fully distributed authorization schemes without
single points of failure.

Using this Library
==================

:doc:`introduction`
    An introduction to cryptographic capabilities.

:doc:`getting-started`
    Getting started with the library.

:doc:`api/lib`
    Generated API documentation.

:doc:`specs`
    List of specifications on which the library is based.


.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   self
   introduction
   getting-started
   examples
   api/lib
   specs
