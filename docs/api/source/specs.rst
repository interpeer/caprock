==============
Specifications
==============

The CAProck library is based on open specifications.

* `draft-jfinkhaeuser-caps-for-distributed-auth <https://specs.interpeer.io/draft-jfinkhaeuser-caps-for-distributed-auth/>`_
  describes the basic principle and challenges of using cryptographic
  capabilities for distributed authorization.
* `draft-jfinkhaeuser-caprock-auth-scheme <https://specs.interpeer.io/draft-jfinkhaeuser-caprock-auth-scheme/>`_
  describes how these principles are applied in the specific CAProck scheme.
* `draft-jfinkhaeuser-caprock-enc-compact <https://specs.interpeer.io/draft-jfinkhaeuser-caprock-enc-compact/>`_
  describes the compact wire encoding that this library produces.

