/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_LIB_TOKEN_H
#define CAPROCK_LIB_TOKEN_H

#include <caprock.h>

#include <optional>

#include <string.h>

#include <s3kr1t/keys.h>
#include <s3kr1t/signatures.h>

#include <liberate/types/varint.h>
#include <liberate/serialization/varint.h>
#include <liberate/cpp/operators.h>
#include <liberate/cpp/hash.h>

#include "key.h"
#include "iso8601.h"
#include "tai64.h"
#include "identifier.h"

namespace caprock {

namespace {

constexpr size_t MAX_CLAIM_ENTRY_SIZE = 512;

inline std::tuple<bool, size_t>
validate_claim_entry(char type, char const * entry, size_t entry_size)
{
  if (!entry) {
    LIBLOG_DEBUG("No claim entry.");
    return {false, 0};
  }

  size_t the_size = entry_size;
  if (!the_size) {
    the_size = strnlen(entry, MAX_CLAIM_ENTRY_SIZE);
  }
  if (!the_size) {
    LIBLOG_DEBUG("Claim is zero sized.");
    return {false, 0};
  }

  // FIXME handle wildcards

  switch (type) {
    case 'P':
      // Predicates are entirely application defined, so we have no opinion
      // here.
      break;

    case 'S':
    case 'O':
      // FIXME tag?
      return validate_identifier_v1(entry, the_size, false);
  }

  // Success
  return {true, the_size};
}



inline std::tuple<bool, size_t>
validate_claim_entry(char type, unsigned char const * entry, size_t entry_size)
{
  return validate_claim_entry(type,
      reinterpret_cast<char const *>(entry),
      entry_size);
}

} // anonymous namespace



inline std::tuple<bool, codec::v1::claim_t>
claim_create_v1(caprock_claim const & input)
{
  codec::v1::claim_t ret;
  size_t size = 0;

  // Subject
  size = codec::v1::decode_identifier(ret.subject, input.subject, input.subject_size);
  if (!size) {
    LIBLOG_DEBUG("Cannot create claim; subject invalid.");
    return {false, {}};
  }

  // Object
  codec::v1::identifier_t obj;
  size = codec::v1::decode_identifier(obj, input.object, input.object_size);
  if (!size) {
    LIBLOG_DEBUG("Cannot create claim; object invalid.");
    return {false, {}};
  }
  ret.object = obj;

  // Predicate is a little different.
  if (!input.predicate) {
    LIBLOG_DEBUG("No predicate given, aborting.");
    return {false, {}};
  }
  size_t predsize = input.predicate_size;
  if (!predsize) {
    predsize = std::strlen(input.predicate);
  }
  ret.predicate = {input.predicate, input.predicate + predsize};

  ret.update_hash();
  return {true, ret};
}



inline std::tuple<bool, caprock_field_identifier>
select_signature(caprock_signature_algorithm sig_alg,
    s3kr1t::key::pair const & kpair)
{
  if (CSA_RAW == sig_alg) {
    LIBLOG_DEBUG("RAW");
    if (s3kr1t::KT_ED != kpair.privkey->type()) {
      return {false, static_cast<caprock_field_identifier>(0)};
    }
    LIBLOG_DEBUG("bits: " << kpair.privkey->bits());
    return {true, static_cast<caprock_field_identifier>(0)};
  }

  if (CSA_SHA2 == sig_alg) {
    // SHA-2 is only acceptable for DSA and ECDSA.
    if (s3kr1t::KT_DSA == kpair.privkey->type()
        || s3kr1t::KT_EC == kpair.privkey->type())
    {
      return {true, CFI_SIGv1_SHA2_64};
    }
    return {false, static_cast<caprock_field_identifier>(0)};
  }

  if (CSA_SHA3 == sig_alg) {
    // SHA-2 is only acceptable for DSA, ECDSA and RSA.
    if (s3kr1t::KT_DSA == kpair.privkey->type()
        || s3kr1t::KT_EC == kpair.privkey->type()
        || s3kr1t::KT_RSA == kpair.privkey->type())
    {
      return {true, CFI_SIGv1_SHA3_64};
    }
    return {false, static_cast<caprock_field_identifier>(0)};
  }

  if (CSA_AUTO == sig_alg) {
    // When auto-selecting, we'll pick hash type as per the spec. The hash size
    // is not specified, so we'll pick the longest (TODO: we may change this?)
    switch (kpair.privkey->type()) {
      case s3kr1t::KT_RSA:
      case s3kr1t::KT_EC:
        return {true, CFI_SIGv1_SHA3_64};

      case s3kr1t::KT_DSA:
        return {true, CFI_SIGv1_SHA2_64};

      case s3kr1t::KT_ED:
        switch (kpair.privkey->bits()) {
          case 25519:
            return {true, CFI_SIGv1_RAW_32};
          case 448:
            return {true, CFI_SIGv1_RAW_57};
          default:
            LIBLOG_ERROR("Unsupported bits for ED key: " << kpair.privkey->bits());
            break;
        }
        break;

      default:
        LIBLOG_DEBUG("Unsupported key type.");
        break;
    }
    return {false, static_cast<caprock_field_identifier>(0)};
  }

  // Unsupported case.
  return {false, static_cast<caprock_field_identifier>(0)};
}


struct token_v1
{
  codec::v1::token_t        payload = {};
  s3kr1t::key::pair         issuer_pair = {};
};


/**
 * Create from API inputs
 */
inline std::tuple<bool, token_v1>
token_create_v1(
    caprock_token_type type,
    caprock_key_pair const * issuer,
    uint64_t sequence_no,
    char const * from,
    char const * to,
    char const * expiry_policy,
    caprock_claim const * claims,
    size_t claims_size,
    caprock_identifier_hash_length issuer_hash_length,
    caprock_signature_algorithm sigalg
)
{
  token_v1 ret;

  // Issuer
  if (!issuer || !issuer->pubkey || !issuer->privkey) {
    LIBLOG_DEBUG("Issuer key pair invalid or not provided.");
    return {false, {}};
  }
  ret.issuer_pair.pubkey = issuer->pubkey->data;
  ret.issuer_pair.privkey = issuer->privkey->data;
  if (!ret.issuer_pair.is_valid()) {
    LIBLOG_DEBUG("Issuer key pair invalid.");
    return {false, {}};
  }

  auto [success, issuer_id] = identifier_from_key_v1(CFI_ISSUER_IDv1,
      *ret.issuer_pair.pubkey, issuer_hash_length);
  if (!success) {
    LIBLOG_DEBUG("Issuer identifier could not be created.");
    return {false, {}};
  }
  ret.payload.issuer = issuer_id;

  // Type
  ret.payload.type = type;

  // Sequence no
  ret.payload.sequence_no = sequence_no;

  // Expiry policy
  if (!expiry_policy) {
    LIBLOG_DEBUG("Scope input values not provided.");
    return {false, {}};
  }

  static size_t CEP_ISSUER_LEN = std::strlen(CAPROCK_EXPIRY_POLICY_ISSUER); // flawfinder: ignore
  static size_t CEP_LOCAL_LEN = std::strlen(CAPROCK_EXPIRY_POLICY_LOCAL); // flawfinder: ignore
  static size_t CEP_MAX_LEN = std::max(CEP_ISSUER_LEN, CEP_LOCAL_LEN);

  size_t expiry_policy_len = strnlen(expiry_policy, CEP_MAX_LEN);
  expiry_policy_len = std::min(expiry_policy_len, CEP_MAX_LEN);
  if (0 == strncmp(expiry_policy, CAPROCK_EXPIRY_POLICY_ISSUER, expiry_policy_len)) {
    ret.payload.scope.expiry_policy = CEP_ISSUER;
  }
  else if (0 == strncmp(expiry_policy, CAPROCK_EXPIRY_POLICY_LOCAL, expiry_policy_len)) {
    ret.payload.scope.expiry_policy = CEP_LOCAL;
  }
  else {
    LIBLOG_DEBUG("Invalid expiry policy provided.");
    return {false, {}};
  }

  // From timestamp
  if (!from) {
    LIBLOG_DEBUG("Scope input values not provided.");
    return {false, {}};
  }
  auto [from_good, from_tp] = caprock::util::parseISO8601(from);
  if (!from_good) {
    LIBLOG_DEBUG("From date could not be parsed.");
    return {false, {}};
  }
  ret.payload.scope.from = from_tp;

  // To timestamp
  if (to) {
    auto [to_good, to_tp_tmp] = caprock::util::parseISO8601(to);
    if (!to_good) {
      LIBLOG_DEBUG("To date could not be parsed.");
      return {false, {}};
    }

    if (to_tp_tmp <= ret.payload.scope.from) {
      LIBLOG_DEBUG("To date must be after from date.");
      return {false, {}};
    }

    ret.payload.scope.to = to_tp_tmp;
  }

  // Claims
  if (!claims || !claims_size) {
    LIBLOG_DEBUG("Need at least one claim to proceed.");
    return {false, {}};
  }

  for (size_t i = 0 ; i < claims_size ; ++i) {
    // XXX LIBLOG_DEBUG("Need to do claim: " << i);
    auto [valid, claim] = claim_create_v1(claims[i]);
    if (!valid) {
      LIBLOG_DEBUG("Invalid claim encountered.");
      return {false, {}};
    }
    ret.payload.claims.push_back(claim);
  }

  // Signature algorithm; need to select for issuer key type.
  auto [sig_ok, sig_type] = select_signature(sigalg, ret.issuer_pair);
  if (!sig_ok) {
    LIBLOG_DEBUG("Signature algorithm does not match key.");
    return {false, {}};
  }
  ret.payload.sig_type = sig_type;

  return {true, ret};
}



inline bool
validate_signature_v1(void const * token, size_t payload_size,
    codec::v1::token_t const & parsed_token, s3kr1t::key const & verifier)
{
  if (!parsed_token.sig_buffer.size()) {
    LIBLOG_ERROR("Insufficient buffer size for verification.");
    return false;
  }
  if (!token || !payload_size) {
    LIBLOG_ERROR("No input to verify.");
    return false;
  }

  // We can skip this pretty fast if the verifier identifier is not the
  // token's issuer. To do this we need to create an identifier for the
  // verifier that is using the same algorithms, etc. as the issuer in
  // the parsed token.
  auto [success, verifier_id] = identifier_from_key_v1(parsed_token.issuer.id_tag,
      parsed_token.issuer.id_type, verifier);
  if (!success) {
    LIBLOG_ERROR("Verifier is invalid or unsuitable.");
    return false;
  }

  if (verifier_id != parsed_token.issuer) {
    LIBLOG_ERROR("Cannot verify with a key that does not match the issuer.");
    return false;
  }

  // Verify
  auto err = s3kr1t::verify(token, payload_size,
      &(parsed_token.sig_buffer[0]), parsed_token.sig_buffer.size(),
      verifier, codec::v1::sig_type_to_digest_type(parsed_token.sig_type));
  if (s3kr1t::ERR_SUCCESS != err) {
    LIBLOG_ERROR("Signature verification error: " << s3kr1t::error_name(err)
        << " // " << s3kr1t::error_message(err));
    return false;
  }

  return true;
}



template <typename durationT = std::chrono::milliseconds>
inline bool
validate_timestamps_v1(codec::v1::token_t const & token,
    caprock::util::time_point<durationT> const & now)
{
  // Do not validate if the timestamps are not matching.
  if (now < token.scope.from) {
    LIBLOG_DEBUG("Token affects the future, rejecting now.");
    return false;
  }
  if (token.scope.to.has_value() && now >= token.scope.to.value()) {
    LIBLOG_DEBUG("Token expired, rejecting now.");
    return false;
  }

  return true;
}


} // namespace caprock

#endif // guard
