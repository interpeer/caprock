/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_LIB_TAI64_H
#define CAPROCK_LIB_TAI64_H

#include <chrono>
#include <iomanip>

#include <liberate/serialization/integer.h>
// #include <liberate/string/hexencode.h>
#include <liberate/logging.h>

#include "iso8601.h"

namespace caprock::util {

namespace {

static constexpr size_t TAI64_SIZE = sizeof(uint64_t);

static constexpr int64_t TAI64_EPOCH = (int64_t{2} << 61) + 10;
static constexpr char TAI64_MAX[TAI64_SIZE] = { // flawfinder: ignore
  '\xff', '\xff', '\xff', '\xff',
  '\xff', '\xff', '\xff', '\xff',
};

inline size_t
serialize_tai64_unix_seconds(char * buffer, size_t bufsize,
    uint64_t unix_seconds)
{
  if (bufsize < TAI64_SIZE) {
    LIBLOG_ERROR("Buffer too small.");
    return 0;
  }

  uint64_t to_serialize = TAI64_EPOCH + unix_seconds;

  char * offset = buffer;
  size_t remaining = bufsize;

  auto used = liberate::serialization::serialize_int(offset, remaining, to_serialize);
  if (used != TAI64_SIZE) {
    LIBLOG_ERROR("Error encoding TAI seconds.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // *offset = '\0';
  // liberate::string::canonical_hexdump hd;
  // LIBLOG_DEBUG("Encoded: " << hd(buffer, used));

  return TAI64_SIZE;
}


inline size_t
deserialize_tai64_unix_seconds(uint64_t & unix_seconds, char const * buffer,
    size_t bufsize)
{
  if (bufsize < TAI64_SIZE) {
    LIBLOG_ERROR("Buffer too small.");
    return 0;
  }

  char const * offset = buffer;
  size_t remaining = bufsize;

  uint64_t deserialized = 0;
  auto used = liberate::serialization::deserialize_int(deserialized, offset, remaining);
  if (!used) {
    LIBLOG_ERROR("Error decoding TAI seconds.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Fix offset
  unix_seconds = deserialized - TAI64_EPOCH;

  return TAI64_SIZE;
}

} // anonymous namespace

template <typename durationT = std::chrono::milliseconds>
inline size_t
serialize_tai64(char * buffer, size_t bufsize,
    time_point<durationT> const & input)
{
  return serialize_tai64_unix_seconds(buffer, bufsize,
      std::chrono::duration_cast<std::chrono::seconds>(input.time_since_epoch())
        .count());
}



inline size_t
serialize_tai64_max(char * buffer, size_t bufsize)
{
  if (bufsize < TAI64_SIZE) {
    return 0;
  }
  std::memcpy(buffer, TAI64_MAX, TAI64_SIZE); // flawfinder: ignore
  return TAI64_SIZE;
}



template <typename durationT = std::chrono::milliseconds>
inline size_t
deserialize_tai64(time_point<durationT> & output,
    char const * buffer, size_t bufsize)
{
  if (bufsize < TAI64_SIZE) {
    LIBLOG_ERROR("Buffer too small.");
    return 0;
  }

  if (0 == std::memcmp(buffer, TAI64_MAX, TAI64_SIZE)) {
    output = time_point<durationT>::max();
    return TAI64_SIZE;
  }

  uint64_t unix_seconds = 0;
  auto used = deserialize_tai64_unix_seconds(unix_seconds, buffer, bufsize);
  if (!used) {
    return 0;
  }

  // time_t is almost certainly integral and seconds since unix epoch, so this
  // does the trick.
  auto tp = std::chrono::system_clock::from_time_t(unix_seconds);
  output = std::chrono::time_point_cast<durationT>(tp);
  return used;
}



} // namespace caprock::util

#endif // guard
