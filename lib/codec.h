/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_LIB_CODEC_H
#define CAPROCK_LIB_CODEC_H

#include <caprock.h>

#include <optional>

#include <caprock/codec.h>

#include <liberate/types/varint.h>
#include <liberate/serialization/varint.h>
#include <liberate/serialization/integer.h>
#include <liberate/cpp/operators.h>
#include <liberate/cpp/hash.h>
#include <liberate/sys/memory.h>

#include <liberate/string/hexencode.h> // FIXME

#include <s3kr1t/signatures.h>

#include <liberate/logging.h>

#include "iso8601.h"
#include "tai64.h"

namespace caprock::codec {

namespace v1 {

namespace {

constexpr inline size_t
sig_type_to_size(caprock_field_identifier const & sig_type)
{
  switch (sig_type) {
    case CFI_SIGv1_SHA2_28:
    case CFI_SIGv1_SHA3_28:
      return 28;

    case CFI_SIGv1_RAW_32:
    case CFI_SIGv1_SHA2_32:
    case CFI_SIGv1_SHA3_32:
      return 32;

    case CFI_SIGv1_SHA2_48:
    case CFI_SIGv1_SHA3_48:
      return 48;

    case CFI_SIGv1_RAW_57:
      return 57;

    case CFI_SIGv1_SHA2_64:
    case CFI_SIGv1_SHA3_64:
      return 64;

    default:
      // Error
      return 0;
  }
}


constexpr inline s3kr1t::digest_type
sig_type_to_digest_type(caprock_field_identifier const & sig_type)
{
  switch (sig_type) {
    case CFI_SIGv1_SHA2_28:
      return s3kr1t::DT_SHA2_224;

    case CFI_SIGv1_SHA2_32:
      return s3kr1t::DT_SHA2_256;

    case CFI_SIGv1_SHA2_48:
      return s3kr1t::DT_SHA2_384;

    case CFI_SIGv1_SHA2_64:
      return s3kr1t::DT_SHA2_512;

    case CFI_SIGv1_SHA3_28:
      return s3kr1t::DT_SHA3_224;

    case CFI_SIGv1_SHA3_32:
      return s3kr1t::DT_SHA3_256;

    case CFI_SIGv1_SHA3_48:
      return s3kr1t::DT_SHA3_384;

    case CFI_SIGv1_SHA3_64:
      return s3kr1t::DT_SHA3_512;

    case CFI_SIGv1_RAW_32:
    case CFI_SIGv1_RAW_57:
    default:
      break;
  }
  return s3kr1t::DT_NONE;
}

} // anonymous namespace

//***** Variable sized BLOB

inline size_t
encode_varblob(char * buf, size_t bufsize, caprock_field_identifier tag,
    char const * input, size_t insize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || !bufsize || bufsize < insize + 1) {
    LIBLOG_DEBUG("Output buffer too small for variable sized BLOB.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Tag
  auto vartmp = static_cast<varint>(tag);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode BLOB tag.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Size
  vartmp = static_cast<varint>(insize);
  used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode claims size.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Data
  if (remaining < insize) {
    LIBLOG_DEBUG("Not enough buffer for data.");
    return 0;
  }
  std::memcpy(offset, input, insize); // flawfinder: ignore
  offset += insize;
  remaining -= insize;

  return (offset - buf);
}



inline std::tuple<size_t, caprock_field_identifier>
decode_varblob(char const * & output, size_t & outsize, char const * buf,
    size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || !bufsize) {
    LIBLOG_DEBUG("Input buffer too small for variable sized BLOB.");
    return {0, {}};
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode tag.");
    return {0, {}};
  }
  auto tag = static_cast<caprock_field_identifier>(vartmp);

  offset += used;
  remaining -= used;

  // Size
  vartmp = static_cast<varint>(0);
  used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode size.");
    return {0, {}};
  }
  auto size = static_cast<size_t>(vartmp);

  offset += used;
  remaining -= used;

  // Data - don't copy, just reference.
  if (size > remaining) {
    LIBLOG_DEBUG("Not enough buffer for BLOB data.");
    return {0, {}};
  }
  output = offset;
  outsize = size;

  offset += size;
  remaining -= size;

  return {(offset - buf), tag};
}


//***** Token Header

inline size_t
encode_token_header(char * buf, size_t bufsize, size_t token_size)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 3) {
    LIBLOG_DEBUG("Buffer too small for token header.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Token tag
  auto vartmp = static_cast<varint>(CFI_TOKENv1);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode that this is a token.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Token size
  if (remaining < 2) {
    LIBLOG_DEBUG("Buffer too small for token header.");
    return 0;
  }

  uint16_t ts = token_size;
  used = serialize_int(offset, remaining, ts);
  if (!used) {
    LIBLOG_DEBUG("Could not encode token size.");
    return 0;
  }

  offset += used;
  remaining -= used;

  return (offset - buf);
}



inline size_t
decode_token_header(size_t & token_size, char const * buf, size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 3) {
    LIBLOG_DEBUG("Buffer too small for token header.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Token tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode token tag.");
    return 0;
  }
  if (vartmp != CFI_TOKENv1) {
    LIBLOG_DEBUG("Expected token tag, but got: " << std::hex << vartmp << std::dec);
    return 0;
  }
  offset += used;
  remaining -= used;

  // Token size
  if (remaining < 2) {
    LIBLOG_DEBUG("Buffer too small for token header.");
    return 0;
  }

  uint16_t ts = 0;
  used = deserialize_int(ts, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode token size.");
    return 0;
  }

  // FIXME minimum token size?
  if (ts < used) {
    LIBLOG_DEBUG("Invalid token size - got: " << ts);
    return 0;
  }
  token_size = ts;

  offset += used;
  remaining -= used;

  return (offset - buf);
}



//***** Token Type

inline size_t
encode_token_type(char * buf, size_t bufsize, caprock_token_type type)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 2) {
    LIBLOG_DEBUG("Buffer too small for token type.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Token tag
  auto vartmp = static_cast<varint>(CFI_TOKEN_TYPEv1);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode that this is a token type.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Token size
  if (remaining < 1) {
    LIBLOG_DEBUG("Buffer too small for token type.");
    return 0;
  }

  *offset = static_cast<char>(type);
  offset += 1;
  remaining -= 1;

  return (offset - buf);
}



inline size_t
decode_token_type(caprock_token_type & type, char const * buf, size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 2) {
    LIBLOG_DEBUG("Buffer too small for token type.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Token tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode token type tag.");
    return 0;
  }
  if (vartmp != CFI_TOKEN_TYPEv1) {
    LIBLOG_DEBUG("Expected token type tag, but got: " << std::hex << vartmp << std::dec);
    return 0;
  }
  offset += used;
  remaining -= used;

  // Token size
  if (remaining < 1) {
    LIBLOG_DEBUG("Buffer too small for token type.");
    return 0;
  }

  type = static_cast<caprock_token_type>(*offset);
  switch (type) {
    case CTT_GRANT:
    case CTT_REVOKE:
      break;

    default:
      LIBLOG_DEBUG("Invalid token type; got: " << std::hex << type << std::dec);
      return 0;
  }

  offset += 1;
  remaining -= 1;

  return (offset - buf);
}



//***** Identifiers


struct identifier_t
  : public ::liberate::cpp::comparison_operators<identifier_t>
{
  caprock_field_identifier              id_tag = {};
  caprock_field_identifier              id_type = {};
  std::vector<::liberate::types::byte>  id_buffer = {};


  size_t      hash_cache = 0;

  inline bool is_equal_to(identifier_t const & other) const
  {
    if (hash() != other.hash()) {
      return false;
    }
    if (id_tag != other.id_tag) {
      return false;
    }
    if (id_type != other.id_type) {
      return false;
    }
    if (id_buffer != other.id_buffer) {
      return false;
    }
    return true;
  }


  inline bool is_less_than(identifier_t const & other) const
  {
    if (hash() < other.hash()) {
      return true;
    }
    if (id_tag < other.id_tag) {
      return true;
    }
    if (id_type < other.id_type) {
      return true;
    }
    if (id_buffer < other.id_buffer) {
      return true;
    }
    return false;
  }


  inline size_t hash() const
  {
    return hash_cache;
  }

  inline void update_hash()
  {
    // LIBLOG_DEBUG("Updating identifier hash.");
    size_t value = liberate::cpp::multi_hash(
        id_tag,
        id_type,
        liberate::cpp::range_hash(id_buffer.begin(), id_buffer.end())
    );
    hash_cache = value;
  }
};


inline std::ostream &
operator<<(std::ostream & os, identifier_t const & id)
{
  os << "<[" << static_cast<int>(id.id_tag)
    << "," << static_cast<int>(id.id_type)
    << "] "
    << liberate::string::hexencode(&id.id_buffer[0], id.id_buffer.size())
    << ">";
  return os;
}



inline constexpr
size_t
identifier_size_untagged(caprock_field_identifier tag)
{
  switch (tag) {
    case CFI_IDv1_NONE:
    case CFI_IDv1_WILDCARD:
      return 1;

    case CFI_IDv1_SHA3_28:
      return 1 + 28;

    case CFI_IDv1_RAW_32:
    case CFI_IDv1_SHA3_32:
      return 1 + 32;

    case CFI_IDv1_SHA3_48:
      return 1 + 48;

    case CFI_IDv1_RAW_57:
      return 1 + 57;

    case CFI_IDv1_SHA3_64:
      return 1 + 64;

    default:
      // Other tags, not for identifiers.
      return 0;
  }
}

static constexpr size_t ID_MIN_SIZE_UNTAGGED = identifier_size_untagged(CFI_IDv1_NONE);
static constexpr size_t ID_MAX_SIZE_UNTAGGED = identifier_size_untagged(CFI_IDv1_SHA3_64);



inline size_t
encode_identifier_check(size_t bufsize, identifier_t const & id,
    bool strict_identifier)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (strict_identifier) {
    switch (id.id_tag) {
      case CFI_ISSUER_IDv1:
      case CFI_CLAIMv1_SUBJECT:
      case CFI_CLAIMv1_OBJECT:
        break;

      default:
        LIBLOG_DEBUG("Not an identifier, aborting.");
        return 0;
    }
  }

  size_t target_size = 0;
  switch (id.id_type) {
    case CFI_IDv1_NONE:
    case CFI_IDv1_WILDCARD:
      target_size = 2;
      break;

    case CFI_IDv1_RAW_32:
      target_size = 34;
      break;

    case CFI_IDv1_RAW_57:
      target_size = 59;
      break;

    case CFI_IDv1_SHA3_28:
      target_size = 30;
      break;

    case CFI_IDv1_SHA3_32:
      target_size = 34;
      break;

    case CFI_IDv1_SHA3_48:
      target_size = 50;
      break;

    case CFI_IDv1_SHA3_64:
      target_size = 66;
      break;

    default:
      LIBLOG_DEBUG("Not an identifier type, aborting.");
      return 0;
  }

  if (bufsize < target_size) {
    LIBLOG_DEBUG("Buffer too small for identifier.");
    return 0;
  }

  if (id.id_buffer.size() != target_size - 2) {
    LIBLOG_DEBUG("ID buffer too large for this identifier type.");
    return 0;
  }

  return target_size;
}



inline size_t
encode_identifier_raw(char * buf, size_t bufsize, size_t target_size,
    identifier_t const & id)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  auto offset = buf;
  auto remaining = bufsize;

  // Type Tag
  auto vartmp = static_cast<varint>(id.id_type);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode the identifier type tag.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Identifier
  if (target_size > 0) {
    if (remaining < target_size) {
      LIBLOG_DEBUG("Buffer too small for identifier.");
      return 0;
    }
    std::memcpy(offset, &(id.id_buffer[0]), target_size); // flawfinder: ignore
    offset += target_size;
    remaining -= target_size;
  }

  return (offset - buf);
}



// FIXME maybe unused?
inline size_t
encode_identifier_untagged(char * buf, size_t bufsize, identifier_t const & id)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  auto target_size = encode_identifier_check(bufsize, id, false);
  if (!target_size) {
    return 0;
  }
  target_size -= 2;

  return encode_identifier_raw(buf, bufsize, target_size, id);
}



inline size_t
encode_identifier(char * buf, size_t bufsize, identifier_t const & id)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  auto target_size = encode_identifier_check(bufsize, id, true);
  if (!target_size) {
    return 0;
  }
  target_size -= 2;

  auto offset = buf;
  auto remaining = bufsize;

  // Tag
  auto vartmp = static_cast<varint>(id.id_tag);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode the identifier tag.");
    return 0;
  }
  offset += used;
  remaining -= used;

  return (offset - buf) + encode_identifier_raw(offset, remaining,
      target_size, id);
}



inline size_t
decode_identifier(identifier_t & id, char const * buf, size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (bufsize < 1) {
    LIBLOG_DEBUG("Buffer too small for identifier.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode identifier tag.");
    return 0;
  }
  id.id_tag = static_cast<caprock_field_identifier>(vartmp);
  switch (id.id_tag) {
    case CFI_ISSUER_IDv1:
    case CFI_CLAIMv1_SUBJECT:
    case CFI_CLAIMv1_OBJECT:
      break;

    default:
      LIBLOG_DEBUG("Expected identifier tag, but got: " << std::hex << vartmp << std::dec);
      return 0;
  }
  offset += used;
  remaining -= used;

  // Type Tag
  vartmp = static_cast<varint>(0);
  used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode identifier type tag.");
    return 0;
  }
  id.id_type = static_cast<caprock_field_identifier>(vartmp);
  size_t target_size = 0;
  switch (id.id_type) {
    case CFI_IDv1_NONE:
    case CFI_IDv1_WILDCARD:
      break;

    case CFI_IDv1_RAW_32:
      target_size = 32;
      break;

    case CFI_IDv1_RAW_57:
      target_size = 57;
      break;

    case CFI_IDv1_SHA3_28:
      target_size = 28;
      break;

    case CFI_IDv1_SHA3_32:
      target_size = 32;
      break;

    case CFI_IDv1_SHA3_48:
      target_size = 48;
      break;

    case CFI_IDv1_SHA3_64:
      target_size = 64;
      break;

    default:
      LIBLOG_DEBUG("Expected identifier type tag, but got: " << std::hex << vartmp << std::dec);
      return 0;
  }
  offset += used;
  remaining -= used;

  // Identifier
  if (remaining < target_size) {
    LIBLOG_DEBUG("Need more data to extract identifier; got " << remaining
        << " but need " << target_size << ".");
    return 0;
  }
  id.id_buffer.resize(target_size);
  std::memcpy(&(id.id_buffer[0]), offset, target_size); // flawfinder: ignore
  offset += target_size;
  remaining -= target_size;


  // Ensure the ID has the correct hash
  id.update_hash();
  return (offset - buf);
}



//***** Sequence number

inline size_t
encode_sequence_number(char * buf, size_t bufsize, uint64_t seq_no)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 2) {
    LIBLOG_DEBUG("Buffer too small for sequence number.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Token tag
  auto vartmp = static_cast<varint>(CFI_SEQUENCE_NOv1);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode that this is a sequence number.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Token size
  if (remaining < 1) {
    LIBLOG_DEBUG("Buffer too small for sequence number.");
    return 0;
  }

  vartmp = static_cast<varint>(seq_no);
  used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode sequence number.");
    return 0;
  }

  offset += used;
  remaining -= used;

  return (offset - buf);
}



inline size_t
decode_sequence_number(uint64_t & seq_no, char const * buf, size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 2) {
    LIBLOG_DEBUG("Buffer too small for sequence number.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Token tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode sequence number tag.");
    return 0;
  }
  if (vartmp != CFI_SEQUENCE_NOv1) {
    LIBLOG_DEBUG("Expected sequence number tag, but got: " << std::hex << vartmp << std::dec);
    return 0;
  }
  offset += used;
  remaining -= used;

  // Token size
  if (remaining < 1) {
    LIBLOG_DEBUG("Buffer too small for sequence number.");
    return 0;
  }

  vartmp = static_cast<varint>(0);
  used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode sequence number.");
    return 0;
  }
  seq_no = static_cast<uint64_t>(vartmp);

  offset += used;
  remaining -= used;

  return (offset - buf);
}


//***** Scope

struct scope_t
  : public ::liberate::cpp::comparison_operators<scope_t>
{
  util::time_point<>                from = {};
  std::optional<util::time_point<>> to = {};
  caprock_expiry_policy             expiry_policy = CEP_ISSUER;


  size_t      hash_cache = 0;

  inline bool is_equal_to(scope_t const & other) const
  {
    if (hash() != other.hash()) {
      return false;
    }
    if (from != other.from) {
      return false;
    }
    if (to != other.to) {
      return false;
    }
    if (expiry_policy != other.expiry_policy) {
      return false;
    }
    return true;
  }


  inline bool is_less_than(scope_t const & other) const
  {
    if (hash() < other.hash()) {
      return true;
    }
    if (from < other.from) {
      return true;
    }
    if (to < other.to) {
      return true;
    }
    if (expiry_policy < other.expiry_policy) {
      return true;
    }
    return false;
  }


  inline size_t hash() const
  {
    return hash_cache;
  }

  inline void update_hash()
  {
    // LIBLOG_DEBUG("Updating identifier hash.");
    size_t value = liberate::cpp::multi_hash(
        from.time_since_epoch().count(),
        to.has_value() ? to.value().time_since_epoch().count() : 0,
        expiry_policy
    );
    hash_cache = value;
  }
};

static constexpr size_t SCOPE_SIZE = 1
  + 1 + 8 // from
  + 1 + 8 // to
  + 1 + 1; // policy


inline size_t
encode_scope(char * buf, size_t bufsize, scope_t const & scope)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < SCOPE_SIZE) {
    LIBLOG_DEBUG("Not enough buffer for scope.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Scope tag
  auto vartmp = static_cast<varint>(CFI_SCOPEv1);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode scope tag.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // The canonical form is to encode from, to and then the expiry policy.
  // As the draft says, encoding should be canonical.
  {
    // Encode scope from
    vartmp = static_cast<liberate::types::varint>(CFI_SCOPEv1_FROM);
    used = uleb128_serialize_varint(offset, remaining, vartmp);
    if (!used) {
      LIBLOG_DEBUG("Could not encode scope from tag.");
      return 0;
    }
    offset += used;
    remaining -= used;

    used = util::serialize_tai64(offset, remaining, scope.from);
    if (!used) {
      LIBLOG_DEBUG("Could not encode scope from value.");
      return 0;
    }
    offset += used;
    remaining -= used;
  }

  {
    // Encode scope to
    vartmp = static_cast<liberate::types::varint>(CFI_SCOPEv1_TO);
    used = uleb128_serialize_varint(offset, remaining, vartmp);
    if (!used) {
      LIBLOG_DEBUG("Could not encode scope to tag.");
      return 0;
    }
    offset += used;
    remaining -= used;

    if (scope.to.has_value()) {
      used = util::serialize_tai64(offset, remaining, scope.to.value());
    }
    else {
      used = util::serialize_tai64_max(offset, remaining);
    }
    if (!used) {
      LIBLOG_DEBUG("Could not encode scope to value.");
      return 0;
    }
    offset += used;
    remaining -= used;
  }

  {
    // Encode scope expiry policy
    vartmp = static_cast<liberate::types::varint>(CFI_SCOPEv1_EXPIRY_POLICY);
    used = uleb128_serialize_varint(offset, remaining, vartmp);
    if (!used) {
      LIBLOG_DEBUG("Could not encode scope expiry policy tag.");
      return 0;
    }
    offset += used;
    remaining -= used;

    *offset = static_cast<char>(scope.expiry_policy);
    ++offset;
    --remaining;
  }

  return (offset - buf);
}



inline size_t
decode_scope(scope_t & scope, char const * buf, size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < SCOPE_SIZE) {
    LIBLOG_DEBUG("Not enough buffer for scope.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Scope tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode scope tag.");
    return 0;
  }
  if (vartmp != CFI_SCOPEv1) {
    LIBLOG_DEBUG("Expected scope tag, but got: " << std::hex << vartmp << std::dec);
    return 0;
  }
  offset += used;
  remaining -= used;

  // Decode fields in any order.
  bool from_found = false;
  bool to_found = false;
  bool policy_found = false;

  for (size_t i = 0 ; i < 3 ; ++i) {
    vartmp = static_cast<varint>(0);
    used = uleb128_deserialize_varint(vartmp, offset, remaining);
    if (!used) {
      LIBLOG_DEBUG("Could not decode tag.");
      return 0;
    }
    offset += used;
    remaining -= used;

    switch (static_cast<caprock_field_identifier>(vartmp)) {
      case CFI_SCOPEv1_FROM:
        {
          used = util::deserialize_tai64(scope.from, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode scope from value.");
            return 0;
          }
          offset += used;
          remaining -= used;
        }
        from_found = true;
        break;

      case CFI_SCOPEv1_TO:
        {
          util::time_point<> tmp_tp;
          used = util::deserialize_tai64(tmp_tp, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode scope from value.");
            return 0;
          }
          offset += used;
          remaining -= used;

          if (tmp_tp != util::time_point<>::max()) {
            if (tmp_tp <= scope.from) {
              LIBLOG_DEBUG("To date must be after from date.");
              return 0;
            }
            scope.to = tmp_tp;
          }
        }
        to_found = true;
        break;

      case CFI_SCOPEv1_EXPIRY_POLICY:
        {
          scope.expiry_policy = static_cast<caprock_expiry_policy>(*offset);
          ++offset;
          --remaining;
        }
        policy_found = true;
        break;

      default:
        LIBLOG_DEBUG("Expected scope inner tag, got: " << std::hex << vartmp << std::dec);
        return 0;
    }
  }

  if (!from_found || !to_found || !policy_found) {
    LIBLOG_DEBUG("Did not find the three mandatory scope elments.");
    return 0;
  }

  scope.update_hash();
  return (offset - buf);
}


//***** Claims

struct claim_t
  : public ::liberate::cpp::comparison_operators<claim_t>
{
  identifier_t                subject = {};
  std::string                 predicate = {};
  std::optional<identifier_t> object = {};

  size_t      hash_cache = 0;


  inline bool is_equal_to(claim_t const & other) const
  {
    if (hash() != other.hash()) {
      return false;
    }
    if (subject != other.subject) {
      return false;
    }
    if (predicate != other.predicate) {
      return false;
    }
    if (object != other.object) {
      return false;
    }
    return true;
  }


  inline bool is_less_than(claim_t const & other) const
  {
    if (hash() < other.hash()) {
      return true;
    }
    if (subject < other.subject) {
      return true;
    }
    if (predicate < other.predicate) {
      return true;
    }
    if (object < other.object) {
      return true;
    }
    return false;
  }


  inline size_t hash() const
  {
    return hash_cache;
  }

  inline void update_hash()
  {
    subject.update_hash();
    if (object.has_value()) {
      object.value().update_hash();
    }

    // LIBLOG_DEBUG("Updating claim hash.");
    size_t value = liberate::cpp::multi_hash(
        subject.hash(),
        predicate,
        object.has_value() ? object.value().hash() : 0
    );
    hash_cache = value;
  }
};


inline size_t
encode_claims(char * buf, size_t bufsize, std::vector<claim_t> const & claims)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || !bufsize) {
    LIBLOG_DEBUG("Not enough buffer for claims.");
    return 0;
  }

  if (claims.empty()) {
    LIBLOG_DEBUG("Need claims to encode.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Claims tag
  auto vartmp = static_cast<varint>(CFI_CLAIMSv1);
  auto used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode claims tag.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Number of claims
  vartmp = static_cast<varint>(claims.size());
  used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode claims size.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // The canonical form is to encode subject, predicate, object, in that order.
  for (auto & claim : claims) {
    {
      // Subject
      auto tmpid = claim.subject;
      tmpid.id_tag = CFI_CLAIMv1_SUBJECT;
      used = encode_identifier(offset, remaining, tmpid);
      if (!used) {
        LIBLOG_DEBUG("Could not encode claim subject.");
        return 0;
      }
      offset += used;
      remaining -= used;
    }

    {
      // Predicate
      used = encode_varblob(offset, remaining,
          CFI_CLAIMv1_PREDICATE,
          claim.predicate.c_str(), claim.predicate.size());
      if (!used) {
        LIBLOG_DEBUG("Could not encode claim predicate.");
        return 0;
      }
      offset += used;
      remaining -= used;
    }

    {
      // Object
      identifier_t tmpid;
      if (claim.object.has_value()) {
        tmpid = claim.object.value();
      }
      else {
        tmpid.id_type = CFI_IDv1_NONE;
      }
      tmpid.id_tag = CFI_CLAIMv1_OBJECT;
      used = encode_identifier(offset, remaining, tmpid);
      if (!used) {
        LIBLOG_DEBUG("Could not encode claim object.");
        return 0;
      }
      offset += used;
      remaining -= used;
    }
  }

  return (offset - buf);
}



inline size_t
decode_claims(std::vector<claim_t> & claims, char const * buf, size_t bufsize)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || !bufsize) {
    LIBLOG_DEBUG("Not enough buffer for claims.");
    return 0;
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Scope tag
  auto vartmp = static_cast<varint>(0);
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode claims tag.");
    return 0;
  }
  if (vartmp != CFI_CLAIMSv1) {
    LIBLOG_DEBUG("Expected claims tag, but got: " << std::hex << vartmp << std::dec);
    return 0;
  }
  offset += used;
  remaining -= used;

  // Size
  vartmp = static_cast<varint>(0);
  used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode claims size.");
    return 0;
  }
  auto num_claims = static_cast<size_t>(vartmp);
  if (!num_claims) {
    LIBLOG_DEBUG("No claims in claims field, aborting.");
    return 0;
  }
  offset += used;
  remaining -= used;
  // XXX LIBLOG_DEBUG("Have " << num_claims << " claim(s).");

  // Decode at most as many elements as the size claims
  claims.clear();
  for (size_t ci = 0 ; ci < num_claims ; ++ci) {
    bool subj_found = false;
    bool pred_found = false;
    bool obj_found = false;

    claim_t current;

    // Decode fields in any order.
    for (size_t i = 0 ; i < 3 ; ++i) {
      vartmp = static_cast<varint>(0);
      used = uleb128_deserialize_varint(vartmp, offset, remaining);
      if (!used) {
        LIBLOG_DEBUG("Could not decode tag.");
        return 0;
      }
      // XXX Do *not* advance offset

      switch (static_cast<caprock_field_identifier>(vartmp)) {
        case CFI_CLAIMv1_SUBJECT:
          {
            used = decode_identifier(current.subject, offset, remaining);
            if (!used) {
              LIBLOG_DEBUG("Could not decode claim subject.");
              return 0;
            }
            offset += used;
            remaining -= used;
          }
          subj_found = true;
          break;

        case CFI_CLAIMv1_PREDICATE:
          {
            char const * field = nullptr;
            size_t field_size = 0;
            auto [used2, tag] = decode_varblob(field, field_size, offset,
                remaining);
            if (!used2 || !field || !field_size) {
              LIBLOG_DEBUG("Could not decode claim predicate.");
              return 0;
            }
            current.predicate = std::string{field, field + field_size};

            offset += used2;
            remaining -= used2;
          }
          pred_found = true;
          break;

        case CFI_CLAIMv1_OBJECT:
          {
            identifier_t tmpid;
            used = decode_identifier(tmpid, offset, remaining);
            if (!used) {
              LIBLOG_DEBUG("Could not decode claim object.");
              return 0;
            }
            current.object = tmpid;

            offset += used;
            remaining -= used;
          }
          obj_found = true;
          break;

        default:
          LIBLOG_DEBUG("Expected scope inner tag, got: " << std::hex << vartmp << std::dec);
          return 0;
      }
    }

    if (!subj_found|| !pred_found || !obj_found) {
      LIBLOG_DEBUG("Did not find the three mandatory claim elments.");
      return 0;
    }

    current.update_hash();
    claims.push_back(current);
  }

  return (offset - buf);
}



//***** Token

struct token_t
  : public ::liberate::cpp::comparison_operators<token_t>
{
  caprock_token_type                    type;
  identifier_t                          issuer;
  uint64_t                              sequence_no;
  scope_t                               scope;
  std::vector<claim_t>                  claims;

  caprock_field_identifier              sig_type;
  std::vector<::liberate::types::byte>  sig_buffer;


  size_t      hash_cache = 0;

  inline bool is_equal_to(token_t const & other) const
  {
    if (hash() != other.hash()) {
      return false;
    }
    if (issuer != other.issuer) {
      return false;
    }
    if (sequence_no != other.sequence_no) {
      return false;
    }
    if (scope != other.scope) {
      return false;
    }
    if (claims != other.claims) {
      return false;
    }
    if (sig_type != other.sig_type) {
      return false;
    }
    if (sig_buffer != other.sig_buffer) {
      return false;
    }
    return true;
  }


  inline bool is_less_than(token_t const & other) const
  {
    if (hash() < other.hash()) {
      return true;
    }
    if (issuer < other.issuer) {
      return true;
    }
    if (sequence_no < other.sequence_no) {
      return true;
    }
    if (scope < other.scope) {
      return true;
    }
    if (claims < other.claims) {
      return true;
    }
    if (sig_type < other.sig_type) {
      return true;
    }
    if (sig_buffer < other.sig_buffer) {
      return true;
    }
    return false;
  }


  inline size_t hash() const
  {
    return hash_cache;
  }

  inline void update_hash()
  {
    issuer.update_hash();
    scope.update_hash();
    for (auto & claim : claims) {
      claim.update_hash();
    }

    // LIBLOG_DEBUG("Updating claim hash.");
    size_t value = liberate::cpp::multi_hash(
        type,
        issuer.hash(),
        sequence_no,
        scope.hash(),
        liberate::cpp::range_hash(claims.begin(), claims.end()),
        sig_type,
        liberate::cpp::range_hash(sig_buffer.begin(), sig_buffer.end())
    );
    hash_cache = value;
  }
};


inline std::tuple<size_t, size_t>
encode_token_payload(char * buf, size_t bufsize, token_t const & token)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < 3) {
    LIBLOG_DEBUG("Buffer too small for token, aborting.");
    return {0, 0};
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Keep space for header
  // XXX The token header must encode the token size, but we don't know the
  //     size yet. We know that the header takes 3 bytes, because the tag is
  //     encoded in one byte, while the size is in two bytes.
  offset += 3;
  remaining -= 3;

  // We encode entries in canonical order, starting with the token type.
  auto used = encode_token_type(offset, remaining, token.type);
  if (!used) {
    LIBLOG_DEBUG("Could not encode token type field.");
    return {0, 0};
  }
  offset += used;
  remaining -= used;

  // Issuer ID
  used = encode_identifier(offset, remaining, token.issuer);
  if (!used) {
    LIBLOG_DEBUG("Could not encode issuer ID.");
    return {0, 0};
  }
  offset += used;
  remaining -= used;

  // Sequence number
  used = encode_sequence_number(offset, remaining, token.sequence_no);
  if (!used) {
    LIBLOG_DEBUG("Could not encode sequence number.");
    return {0, 0};
  }
  offset += used;
  remaining -= used;

  // Scope
  used = encode_scope(offset, remaining, token.scope);
  if (!used) {
    LIBLOG_DEBUG("Could not encode scope.");
    return {0, 0};
  }
  offset += used;
  remaining -= used;

  // Claims
  used = encode_claims(offset, remaining, token.claims);
  if (!used) {
    LIBLOG_DEBUG("Could not encode claims.");
    return {0, 0};
  }
  offset += used;
  remaining -= used;

  // Signature
  // We do not know what the signature buffer contains. However, we know the
  // size it should have, based on the sig_type field.
  size_t sig_size = sig_type_to_size(token.sig_type);
  if (!sig_size) {
    LIBLOG_DEBUG("Expected signature type, got: " << std::hex << token.sig_type << std::dec);
    return {0, 0};
  }

  auto vartmp = static_cast<varint>(token.sig_type);
  used = uleb128_serialize_varint(offset, remaining, vartmp);
  if (!used) {
    LIBLOG_DEBUG("Could not encode signature tag.");
    return {0, 0};
  }
  auto payload_offset = offset;
  offset += used;
  remaining -= used;

  if (remaining < sig_size) {
    LIBLOG_DEBUG("Not enough buffer for signature.");
    return {0, 0};
  }
  liberate::sys::secure_memzero(offset, sig_size);
  offset += sig_size;
  remaining -= sig_size;

  // Now we have the token (and payload) size, we can encode the token header
  size_t payload_size = payload_offset - buf;
  size_t token_size = offset - buf;

  used = encode_token_header(buf, bufsize, token_size);
  if (used != 3) {
    LIBLOG_DEBUG("Header is not encoded in the expected size!");
    return {0, 0};
  }

  return {token_size, payload_size};
}


inline size_t
encode_token_signature(char * sig_buf, size_t sig_size,
    token_t const & token,
    s3kr1t::key::pair const & issuer_pair,
    char const * payload_buf, size_t payload_size)
{
  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!sig_buf || sig_size < 33) {
    LIBLOG_DEBUG("Buffer too small for signature, aborting.");
    return 0;
  }

  if (!payload_buf || !payload_size) {
    LIBLOG_DEBUG("No payload to sign, aborting!");
    return 0;
  }

  auto offset = sig_buf;
  auto remaining = sig_size;

  // Ensure we're at the right offset. This also moves our offset to the
  // exact right position.
  varint vartmp;
  auto used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_ERROR("Could not deseralize signature type, aborting.");
    return 0;
  }
  if (vartmp != token.sig_type) {
    LIBLOG_ERROR("Signature type mismatch, aborting!");
    return 0;
  }
  offset += used;
  remaining -= used;

  // The sig_size passed to the function is the available buffer. We now
  // need to determine the size we need.
  auto req_sig_size = sig_type_to_size(token.sig_type);
  if (!req_sig_size) {
    LIBLOG_ERROR("Could not determine signature size, aborting!");
    return 0;
  }
  if (remaining < req_sig_size) {
    LIBLOG_ERROR("Not enough space for signature, aborting!");
    return 0;
  }

  // Let's determine the digest type.
  auto dtype = sig_type_to_digest_type(token.sig_type);

  // Now sign.
  size_t required = 0;
  auto serr = s3kr1t::sign(offset, remaining, required,
      payload_buf, payload_size,
      *issuer_pair.privkey,
      dtype);
  if (s3kr1t::ERR_SUCCESS != serr) {
    LIBLOG_ERROR("Failed to write signature: " << s3kr1t::error_name(serr)
        << " // " << s3kr1t::error_message(serr));
    return 0;
  }
  offset += required;
  remaining -= required;

  return offset - sig_buf;
}



inline std::tuple<size_t, size_t>
decode_token(token_t & token, char const * buf, size_t bufsize)
{
  // Note: we don't need to be accurate here. However, the token must contain
  // enough elements that there should be at least ten token tags, if nothing
  // else.
  static constexpr size_t MIN_TOKEN_SIZE = 10;

  using namespace liberate::serialization;
  using namespace liberate::types;

  if (!buf || bufsize < MIN_TOKEN_SIZE) {
    LIBLOG_DEBUG("Buffer too small for token, aborting.");
    return {0, 0};
  }

  auto offset = buf;
  auto remaining = bufsize;

  // Try decoding the header. That should give us an indication of the size.
  size_t token_size = 0;
  auto used = decode_token_header(token_size, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Unable to decode token header.");
    return {0, 0};
  }
  offset += used;
  remaining -= used;

  if (token_size < MIN_TOKEN_SIZE) {
    LIBLOG_DEBUG("Invalid token header.");
    return {0, 0};
  }
  if (token_size > bufsize) {
    LIBLOG_DEBUG("Buffer too small to contain token, aborting.");
    return {0, 0};
  }

  // We need to grab a type, issuer, sequence number, scope and claims. That's
  // five entries in any order.
  bool type_found = false;
  bool issuer_found = false;
  bool seqno_found = false;
  bool scope_found = false;
  bool claims_found = false;

  for (size_t i = 0 ; i < 5 ; ++i) {
    auto vartmp = static_cast<varint>(0);
    used = uleb128_deserialize_varint(vartmp, offset, remaining);
    if (!used) {
      LIBLOG_DEBUG("Could not decode tag.");
      return {0, 0};
    }
    // XXX Do *not* advance offset

    switch (static_cast<caprock_field_identifier>(vartmp)) {
      case CFI_TOKEN_TYPEv1:
        {
          used = decode_token_type(token.type, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode token type.");
            return {0, 0};
          }
          if (token.type != CTT_GRANT && token.type != CTT_REVOKE) {
            LIBLOG_DEBUG("Invalid token type value!");
            return {0, 0};
          }
          offset += used;
          remaining -= used;
        }
        type_found = true;
        break;

      case CFI_ISSUER_IDv1:
        {
          used = decode_identifier(token.issuer, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode token issuer.");
            return {0, 0};
          }
          switch (token.issuer.id_type) {
            case CFI_IDv1_RAW_32:
            case CFI_IDv1_RAW_57:
            case CFI_IDv1_SHA3_28:
            case CFI_IDv1_SHA3_32:
            case CFI_IDv1_SHA3_48:
            case CFI_IDv1_SHA3_64:
              break;

            default:
              LIBLOG_DEBUG("Got invalid issuer identifier type.");
              return {0, 0};
          }
          offset += used;
          remaining -= used;
        }
        issuer_found = true;
        break;

      case CFI_SEQUENCE_NOv1:
        {
          used = decode_sequence_number(token.sequence_no, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode token sequence number.");
            return {0, 0};
          }
          offset += used;
          remaining -= used;
        }
        seqno_found = true;
        break;

      case CFI_SCOPEv1:
        {
          used = decode_scope(token.scope, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode token scope.");
            return {0, 0};
          }
          offset += used;
          remaining -= used;
        }
        scope_found = true;
        break;

      case CFI_CLAIMSv1:
        {
          used = decode_claims(token.claims, offset, remaining);
          if (!used) {
            LIBLOG_DEBUG("Could not decode token claims.");
            return {0, 0};
          }
          offset += used;
          remaining -= used;
        }
        claims_found = true;
        break;
 
      default:
        LIBLOG_DEBUG("Expected valid inner tag, got: " << std::hex << vartmp << std::dec);
        return {0, 0};
    }
  }

  if (!type_found || !issuer_found || !seqno_found || !scope_found || !claims_found) {
    LIBLOG_DEBUG("Did not find all the mandatory token elments.");
    return {0, 0};
  }

  // Signature
  auto payload = offset - buf;

  auto vartmp = static_cast<varint>(0);
  used = uleb128_deserialize_varint(vartmp, offset, remaining);
  if (!used) {
    LIBLOG_DEBUG("Could not decode signature tag.");
    return {0, 0};
  }
  token.sig_type = static_cast<caprock_field_identifier>(vartmp);
  size_t sig_size = sig_type_to_size(token.sig_type);
  if (!sig_size) {
      LIBLOG_DEBUG("Expected signature type, got: " << std::hex << token.sig_type << std::dec);
      return {0, 0};
  }
  offset += used;
  remaining -= used;

  if (remaining < sig_size) {
    LIBLOG_DEBUG("Not enough buffer for signature.");
    return {0, 0};
  }

  token.sig_buffer.resize(sig_size);
  std::memcpy(&(token.sig_buffer[0]), offset, sig_size); // flawfinder: ignore

  offset += sig_size;
  remaining -= sig_size;

  // Check token_size
  size_t total_used = offset - buf;
  if (total_used != token_size) {
    LIBLOG_DEBUG("Encoded token size " << token_size << " does not match the "
        "size used: " << total_used);
    return {0, 0};
  }

  token.update_hash();
  return {total_used, payload};
}



} // namespace v1


} // namespace caprock::codec

LIBERATE_MAKE_HASHABLE(caprock::codec::v1::identifier_t);
LIBERATE_MAKE_HASHABLE(caprock::codec::v1::scope_t);
LIBERATE_MAKE_HASHABLE(caprock::codec::v1::claim_t);
LIBERATE_MAKE_HASHABLE(caprock::codec::v1::token_t);

#endif // guard
