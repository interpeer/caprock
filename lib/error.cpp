/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <caprock/error.h>

/**
 * Stringify the symbol passed to CAPROCK_SPRINGIFY()
 **/
#define CAPROCK_STRINGIFY(n) CAPROCK_STRINGIFY_HELPER(n)
#define CAPROCK_STRINGIFY_HELPER(n) #n

/**
 * (Un-)define macros to ensure error defintions are being generated.
 **/
#define CAPROCK_START_ERRORS \
  static const struct { \
    char const * const name; \
    caprock_error_t code; \
    char const * const message; \
  } _caprock_error_table[] = {
#define CAPROCK_ERRDEF(name, code, desc) \
  { "CAPROCK_" CAPROCK_STRINGIFY(name), code, desc },
#define CAPROCK_END_ERRORS { nullptr, 0, nullptr } };

#undef CAPROCK_ERROR_H
#include <caprock/error.h>



/*****************************************************************************
 * Functions
 **/

extern "C" {

char const *
caprock_error_message(caprock_error_t code)
{
  if (code >= CAPROCK_ERROR_LAST) {
    return "unidentified error";
  }

  for (size_t i = 0
      ; i < sizeof(_caprock_error_table) / sizeof(_caprock_error_table[0])
      ; ++i)
  {
    if (code == _caprock_error_table[i].code) {
      return _caprock_error_table[i].message;
    }
  }

  return "unidentified error";
}



char const *
caprock_error_name(caprock_error_t code)
{
  if (code >= CAPROCK_ERROR_LAST) {
    return "unidentified error";
  }

  for (size_t i = 0
      ; i < sizeof(_caprock_error_table) / sizeof(_caprock_error_table[0])
      ; ++i)
  {
    if (code == _caprock_error_table[i].code) {
      return _caprock_error_table[i].name;
    }
  }

  return "unidentified error";
}

} // extern "C"
