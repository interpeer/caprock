/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <caprock/keys.h>
#include <caprock/codec.h>

#include <memory>
#include <cstring>

#include <s3kr1t/keys.h>

#include <liberate/serialization/varint.h>
#include <liberate/logging.h>

#include "key.h"
#include "identifier.h"

extern "C" {

void caprock_key_init(caprock_key ** key)
{
  if (!key) {
    return;
  }

  caprock_key_free(key);

  *key = new caprock_key;
}


caprock_key_pair caprock_key_init_pair()
{
  return {};
}



int caprock_key_is_valid(caprock_key const * key)
{
  return (key && key->data && *(key->data)) ? 1 : 0;
}



int caprock_key_is_public(caprock_key const * key)
{
  return (key && key->data && key->data->is_public()) ? 1 : 0;
}



int caprock_key_is_private(caprock_key const * key)
{
  return (key && key->data && key->data->is_private()) ? 1 : 0;
}



caprock_error_t
caprock_key_read_public(caprock_key * pubkey, void const * buffer, size_t bufsize,
    char const * password)
{
  if (!pubkey || !buffer || !bufsize) {
    LIBLOG_DEBUG("Missing input values.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  auto tmp = s3kr1t::key::read_pubkey(buffer, bufsize,
      password ? std::string{password} : std::string{});
  if (!tmp || !tmp.is_public()) {
    LIBLOG_DEBUG("The provided key is not a public key.");
    return CAPROCK_ERR_INVALID_KEY;
  }

  pubkey->data = std::make_shared<s3kr1t::key>(tmp);
  return CAPROCK_ERR_SUCCESS;
}


caprock_error_t
caprock_key_read_private(caprock_key * privkey, void const * buffer, size_t bufsize,
    char const * password)
{
  if (!privkey || !buffer || !bufsize) {
    LIBLOG_DEBUG("Missing input values.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  auto tmp = s3kr1t::key::read_privkey(buffer, bufsize,
      password ? std::string{password} : std::string{});
  if (!tmp || !tmp.is_private()) {
    LIBLOG_DEBUG("The provided key is not a private key.");
    return CAPROCK_ERR_INVALID_KEY;
  }

  privkey->data = std::make_shared<s3kr1t::key>(tmp);
  return CAPROCK_ERR_SUCCESS;
}



caprock_error_t
caprock_key_read_pair(caprock_key_pair * pair, void const * buffer, size_t bufsize,
    char const * password)
{
  if (!pair || !buffer || !bufsize) {
    LIBLOG_DEBUG("Missing input values.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  auto tmp = s3kr1t::key::read_keys(buffer, bufsize,
      password ? std::string{password} : std::string{});
  if (!tmp.is_valid()) {
    LIBLOG_DEBUG("The provided key is not a valid key pair.");
    return CAPROCK_ERR_INVALID_KEY;
  }

  if (!pair->pubkey) {
    pair->pubkey = new caprock_key;
  }
  if (!pair->privkey) {
    pair->privkey = new caprock_key;
  }

  pair->pubkey->data = tmp.pubkey;
  pair->privkey->data = tmp.privkey;

  return CAPROCK_ERR_SUCCESS;
}


caprock_error_t
caprock_key_extract_public(caprock_key ** pubkey, caprock_key_pair const * pair)
{
  if (!pubkey || !pair) {
    LIBLOG_DEBUG("Missing input values.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  if (*pubkey) {
    LIBLOG_WARN("Overwriting existing pubkey.");
    caprock_key_free(pubkey);
  }

  *pubkey = new caprock_key;
  (*pubkey)->data = pair->pubkey->data;

  return CAPROCK_ERR_SUCCESS;
}




caprock_error_t
caprock_key_identifier_public(
    caprock_field_identifier tag,
    void * buffer, size_t * bufsize,
    caprock_key const * pubkey,
    caprock_identifier_hash_length hash_length)
{
  if (!buffer || !bufsize || !*bufsize || !pubkey || !pubkey->data) {
    LIBLOG_DEBUG("Missing input values.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  size_t remaining = *bufsize;
  *bufsize = 0;

  auto used = caprock::serialize_identifier_from_key_v1(tag,
      buffer, remaining,
      *pubkey, hash_length);
  if (!used) {
    LIBLOG_DEBUG("Could not encode identifier.");
    return CAPROCK_ERR_CODEC;
  }

  *bufsize = used;
  return CAPROCK_ERR_SUCCESS;
}



caprock_error_t
caprock_key_identifier_pair(
    caprock_field_identifier tag,
    void * buffer, size_t * bufsize,
    caprock_key_pair const * pair,
    caprock_identifier_hash_length hash_length)
{
  if (!pair || !pair->pubkey) {
    LIBLOG_DEBUG("Missing input values.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  return caprock_key_identifier_public(tag, buffer, bufsize, pair->pubkey,
      hash_length);
}



void
caprock_key_free(caprock_key ** key)
{
  if (!key || !*key) {
    return;
  }
  (*key)->data = nullptr;
  delete *key;
  *key = nullptr;
}



void
caprock_key_free_pair(caprock_key_pair * pair)
{
  if (!pair) {
    return;
  }

  if (pair->pubkey) {
    caprock_key_free(&(pair->pubkey));
  }

  if (pair->privkey) {
    caprock_key_free(&(pair->privkey));
  }
}


} // extern "C"
