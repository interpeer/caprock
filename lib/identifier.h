/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_LIB_IDENTIFIER_H
#define CAPROCK_LIB_IDENTIFIER_H

#include <caprock.h>

#include <caprock/codec.h>

#include <s3kr1t/keys.h>

#include <liberate/serialization/varint.h>
#include <liberate/logging.h>

#include "key.h"
#include "codec.h"

namespace caprock {

inline constexpr
s3kr1t::digest_type
digest_from_type_v1(caprock_field_identifier type)
{
  switch (type) {
    case CFI_IDv1_SHA3_28:
      return s3kr1t::DT_SHA3_224;

    case CFI_IDv1_SHA3_32:
      return s3kr1t::DT_SHA3_256;

    case CFI_IDv1_SHA3_48:
      return s3kr1t::DT_SHA3_384;

    case CFI_IDv1_SHA3_64:
      return s3kr1t::DT_SHA3_512;

    default:
      break;
  }

  return s3kr1t::DT_NONE;
}


inline constexpr
std::tuple<s3kr1t::digest_type, caprock_field_identifier>
identifier_v1_meta(caprock_identifier_hash_length hash_length)
{
  s3kr1t::digest_type digest = s3kr1t::DT_NONE;
  auto tag = static_cast<caprock_field_identifier>(0);

  switch (hash_length) {
    case CIH_224:
      digest = s3kr1t::DT_SHA3_224;
      tag = CFI_IDv1_SHA3_28;
      break;

    case CIH_256:
      digest = s3kr1t::DT_SHA3_256;
      tag = CFI_IDv1_SHA3_32;
      break;

    case CIH_384:
      digest = s3kr1t::DT_SHA3_384;
      tag = CFI_IDv1_SHA3_48;
      break;

    case CIH_512:
      digest = s3kr1t::DT_SHA3_512;
      tag = CFI_IDv1_SHA3_64;
      break;

    default:
      // not possible in constexpr function
      // LIBLOG_ERROR("Unknown hash length parameter, aborting!");
      break;
  }

  return {digest, tag};
}




inline constexpr
std::tuple<s3kr1t::digest_type, caprock_field_identifier>
identifier_v1_meta(s3kr1t::key_type ktype,
    caprock_identifier_hash_length hash_length,
    size_t bits)
{
  s3kr1t::digest_type digest = s3kr1t::DT_NONE;
  auto tag = static_cast<caprock_field_identifier>(0);

  switch (hash_length) {
    case CIH_ANY:
      // Handled later
      break;

    case CIH_224:
      digest = s3kr1t::DT_SHA3_224;
      tag = CFI_IDv1_SHA3_28;
      break;

    case CIH_256:
      digest = s3kr1t::DT_SHA3_256;
      tag = CFI_IDv1_SHA3_32;
      break;

    case CIH_384:
      digest = s3kr1t::DT_SHA3_384;
      tag = CFI_IDv1_SHA3_48;
      break;

    case CIH_512:
      digest = s3kr1t::DT_SHA3_512;
      tag = CFI_IDv1_SHA3_64;
      break;

    default:
      // not possible in constexpr function
      // LIBLOG_ERROR("Unknown hash length parameter, aborting!");
      return {digest, tag};
  }

  // Handle CIH_ANY based on key type.
  if (digest == s3kr1t::DT_NONE) {
    if (ktype == s3kr1t::KT_ED) {
      if (bits == 25519) {
        tag = CFI_IDv1_RAW_32;
      }
      else if (bits == 448) {
        tag = CFI_IDv1_RAW_57;
      }
    }
    else {
      // Default - assume CIH_512
      digest = s3kr1t::DT_SHA3_512;
      tag = CFI_IDv1_SHA3_64;
    }
  }

  return {digest, tag};
}



inline std::tuple<bool, codec::v1::identifier_t>
identifier_from_key_v1(
    caprock_field_identifier tag,
    s3kr1t::key const & key,
    caprock_identifier_hash_length hash_length)
{
  if (!key || !key.is_public()) {
    LIBLOG_DEBUG("Key is not valid or not a public key.");
    return {false, {}};
  }

  switch (tag) {
    case CFI_ISSUER_IDv1:
    case CFI_CLAIMv1_SUBJECT:
    case CFI_CLAIMv1_OBJECT:
      break;

    default:
      LIBLOG_DEBUG("Invalid identifier tag.");
      return {false, {}};
  }

  auto [digest, type] = identifier_v1_meta(key.type(), hash_length, key.bits());
  if (!type) {
    LIBLOG_DEBUG("Unsupported key type, aborting: "
        << static_cast<int>(key.type()) << " - " << hash_length << " - "
        << key.bits());
    return {false, {}};
  }

  // Pre-polulate ID.
  codec::v1::identifier_t id;
  id.id_tag = tag;
  id.id_type = type;

  if (s3kr1t::DT_NONE == digest) {
    // We want to encode a raw key. Since there are only for edwards curve
    // keys, we know the maximum size is 57 bytes.
    id.id_buffer.resize(57);
    size_t written = 0;
    auto err = key.write(&(id.id_buffer[0]), id.id_buffer.size(),
        s3kr1t::KE_RAW, written);
    if (s3kr1t::ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Could not write raw key, aborting.");
      return {false, {}};
    }
    id.id_buffer.resize(written);
  }
  else {
    // Otherwise, we want to encode a hash of the given digest type.
    id.id_buffer = key.hash(digest);
  }

  id.update_hash();
  return {true, id};
}



inline std::tuple<bool, codec::v1::identifier_t>
identifier_from_key_v1(
    caprock_field_identifier tag,
    caprock_field_identifier type,
    s3kr1t::key const & key)
{
  if (!key || !key.is_public()) {
    LIBLOG_DEBUG("Key is not valid or not a public key.");
    return {false, {}};
  }

  switch (tag) {
    case CFI_ISSUER_IDv1:
    case CFI_CLAIMv1_SUBJECT:
    case CFI_CLAIMv1_OBJECT:
      break;

    default:
      LIBLOG_DEBUG("Invalid identifier tag.");
      return {false, {}};
  }

  auto digest = digest_from_type_v1(type);

  // Pre-polulate ID.
  codec::v1::identifier_t id;
  id.id_tag = tag;
  id.id_type = type;

  if (s3kr1t::DT_NONE == digest) {
    // We want to encode a raw key. Since there are only for edwards curve
    // keys, we know the maximum size is 57 bytes.
    id.id_buffer.resize(57);
    size_t written = 0;
    auto err = key.write(&(id.id_buffer[0]), id.id_buffer.size(),
        s3kr1t::KE_RAW, written);
    if (s3kr1t::ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Could not write raw key, aborting.");
      return {false, {}};
    }
    id.id_buffer.resize(written);
  }
  else {
    // Otherwise, we want to encode a hash of the given digest type.
    id.id_buffer = key.hash(digest);
  }

  id.update_hash();
  return {true, id};
}



inline std::tuple<bool, codec::v1::identifier_t>
object_identifier_from_name_v1(
    void const * name, size_t name_size,
    caprock_identifier_hash_length hash_length)
{
  auto [digest, type] = identifier_v1_meta(hash_length);
  if (!type) {
    LIBLOG_DEBUG("Unsupported hash length, aborting.");
    return {false, {}};
  }

  // Pre-polulate ID.
  codec::v1::identifier_t id;
  id.id_tag = CFI_CLAIMv1_OBJECT;
  id.id_type = type;

  // Create digest - we allocate enough buffer for the largest digest size,
  // just in case.
  id.id_buffer.resize(64);
  size_t used = id.id_buffer.size();
  auto err = s3kr1t::digest(&id.id_buffer[0], id.id_buffer.size(), used,
      name, name_size, digest);
  if (s3kr1t::ERR_SUCCESS != err) {
    LIBLOG_DEBUG("Error creating digest, aborting.");
    return {false, {}};
  }
  id.id_buffer.resize(used);

  id.update_hash();
  return {true, id};
}



inline size_t
serialize_identifier_from_key_v1(
    caprock_field_identifier tag,
    void * output, size_t output_size,
    s3kr1t::key const & key,
    caprock_identifier_hash_length hash_length)
{
  auto [success, id] = identifier_from_key_v1(tag, key, hash_length);
  if (!success) {
    return 0;
  }

  // Encode and return
  return codec::v1::encode_identifier(static_cast<char *>(output), output_size,
      id);
}



inline size_t
serialize_identifier_from_key_v1(
    caprock_field_identifier tag,
    void * output, size_t output_size,
    caprock_key const & key,
    caprock_identifier_hash_length hash_length)
{
  if (!key.data) {
    LIBLOG_DEBUG("No key data.");
    return 0;
  }
  return serialize_identifier_from_key_v1(tag, output, output_size,
      *key.data, hash_length);
}



inline size_t
serialize_object_identifier_from_name(
    void * output, size_t output_size,
    void const * input, size_t input_size,
    caprock_identifier_hash_length hash_length)
{
  auto [success, id] = object_identifier_from_name_v1(input, input_size,
      hash_length);
  if (!success) {
    return 0;
  }

  // Encode and return
  return codec::v1::encode_identifier(static_cast<char *>(output), output_size,
      id);
}




template <typename T>
inline std::tuple<bool, size_t>
validate_identifier_v1(T const * buf, size_t size, bool strict_size = true)
{
  namespace cv1 = caprock::codec::v1;
  static_assert(sizeof(T) == 1, "Only single byte types allowed.");

  // Check size boundaries
  if (strict_size) {
    if (size < cv1::ID_MIN_SIZE_UNTAGGED || size > cv1::ID_MAX_SIZE_UNTAGGED) {
      LIBLOG_DEBUG("Identifier size is invalid: " << size);
      return {false, 0};
    }
  }

  // Check identifier tag
  auto expected_size = cv1::identifier_size_untagged(
      static_cast<caprock_field_identifier>(buf[0])
  );
  if (!expected_size) {
    LIBLOG_DEBUG("Identifier type is bad.");
    return {false, 0};
  }

  if (size < expected_size) {
    LIBLOG_DEBUG("Size is bad: got " << size << " but expected "
        << expected_size);
  }
  return {true, expected_size};
}


template <typename T>
inline std::tuple<bool, size_t>
validate_identifier_v1(std::vector<T> const & id, bool strict_size = true)
{
  return validate_identifier_v1(id.data(), id.size(), strict_size);
}



} // namespace caprock

#endif // guard
