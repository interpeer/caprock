/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CAPROCK_LIB_ISO8601_H
#define CAPROCK_LIB_ISO8601_H

#include <sstream>
#include <tuple>

#include <date/date.h>

namespace caprock::util {

template <typename durationT = std::chrono::milliseconds>
using time_point = std::chrono::time_point<std::chrono::system_clock, durationT>;


/**
 * Parse ISO8601 formatted timestamp into chrono milliseconds.
 *
 * Note that up until C++20, it was not clearly defined, but gnerally
 * assumed that the system_clock we're converting to here was measuring
 * time *without* leap seconds since the Unix Epoch. Since C++20, this is
 * the defined behaviour. We assume this behaviour here in either case.
 *
 * This means that serializing and deserializing time_point to TAI64
 * (see tai64.h) does *not* require leap second adjustment, because leap
 * seconds are a UTC concern. Instead, we rely on the date library to add
 * and remove leap seconds from the ISO8601 time stamp.
 */
template <typename durationT = std::chrono::milliseconds>
inline std::tuple<
  bool,
  time_point<durationT>
>
parseISO8601(std::string const & input)
{
  time_point<durationT> tp;

  std::istringstream in{input};
  in >> date::parse("%FT%TZ", tp);

  if (!in.fail()) {
    return {true, tp};
  }

  in.clear();
  in.str(input);
  in >> date::parse("%FT%T%Ez", tp);

  return {!in.fail(), tp};
}


} // namespace caprock::util

#endif // guard
