/**
 * This file is part of caprock.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <caprock/basics.h>
#include <caprock/codec.h>

#include <cstring>
#include <map>

#include <liberate/logging.h>

#include "token.h"
#include "iso8601.h"

namespace {

inline std::tuple<caprock_error_t, caprock::codec::v1::token_t>
parse_and_validate_token(void const * token, size_t token_size,
    caprock::util::time_point<> const & now,
    caprock_key const & verifier)
{
  if (!token || !token_size) {
    LIBLOG_DEBUG("Missing inputs.");
    return {CAPROCK_ERR_INVALID_VALUE, {}};
  }

  // Decode the token
  caprock::codec::v1::token_t parsed;
  auto [used, payload] = caprock::codec::v1::decode_token(parsed,
      static_cast<char const *>(token), token_size);
  if (!used) {
    LIBLOG_DEBUG("Unable to parse token.");
    return {CAPROCK_ERR_CODEC, {}};
  }

  // Validate signature
  auto success = caprock::validate_signature_v1(token,
      payload, parsed, *(verifier.data));
  if (!success) {
    LIBLOG_DEBUG("Could not validate signature.");
    return {CAPROCK_ERR_VALIDATION, {}};
  }

  // Validate timestamps
  success = caprock::validate_timestamps_v1(parsed, now);
  if (!success) {
    LIBLOG_DEBUG("Token validity period mismatch.");
    return {CAPROCK_ERR_VALIDATION, {}};
  }

  return {CAPROCK_ERR_SUCCESS, parsed};
}


} // anonymous namespace

extern "C" {

char const * const CAPROCK_EXPIRY_POLICY_ISSUER = "issuer";
char const * const CAPROCK_EXPIRY_POLICY_LOCAL = "local";
char const * const CAPROCK_CLAIMS_WILDCARD = "*";

size_t const CAPROCK_IDENTIFIER_MIN_SIZE = caprock::codec::v1::ID_MIN_SIZE_UNTAGGED;
size_t const CAPROCK_IDENTIFIER_MAX_SIZE = caprock::codec::v1::ID_MAX_SIZE_UNTAGGED;


caprock_error_t
caprock_grant_create(
    void * buffer, size_t * bufsize,
    caprock_key_pair const * issuer,
    uint64_t sequence_no,
    char const * from,
    char const * to,
    char const * expiry_policy,
    caprock_claim const * claims,
    size_t claims_size,
    caprock_identifier_hash_length issuer_hash_length,
    caprock_signature_algorithm sigalg
)
{
  // *** Input validation & parsing (because the two often execute
  //     similar paths)
  if (!buffer || !bufsize || !*bufsize) {
    LIBLOG_DEBUG("Output buffer too small or not provided.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // *** Basic token payload
  auto [success, token] = caprock::token_create_v1(
      CTT_GRANT, issuer, sequence_no,
      from, to, expiry_policy,
      claims, claims_size,
      issuer_hash_length, sigalg
  );
  if (!success) {
    LIBLOG_DEBUG("Could not create valid token from input.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // *** Serialize token into buffer
  char * offset = static_cast<char *>(buffer);
  size_t remaining = *bufsize;

  auto [used, payload] = caprock::codec::v1::encode_token_payload(offset, remaining,
      token.payload);
  if (!used) {
    LIBLOG_DEBUG("Could not serialize token.");
    return CAPROCK_ERR_CODEC;
  }

  // Add signature - the tag is already in place, but the signature itself
  // is missing.
  offset += payload;
  remaining -= payload;

  size_t payload_size = offset - static_cast<char *>(buffer);

  used = caprock::codec::v1::encode_token_signature(offset, remaining,
      token.payload, token.issuer_pair,
      static_cast<char *>(buffer), payload_size);
  if (!used) {
    LIBLOG_DEBUG("Could not add signature.");
    return CAPROCK_ERR_CODEC;
  }

  offset += used;
  remaining -= used;

  *bufsize = offset - static_cast<char *>(buffer);
  return CAPROCK_ERR_SUCCESS;
}



caprock_error_t
caprock_revocation_create(
    void * buffer, size_t * bufsize,
    caprock_key_pair const * issuer,
    uint64_t sequence_no,
    char const * from,
    char const * to,
    char const * expiry_policy,
    caprock_claim const * claims,
    size_t claims_size,
    caprock_identifier_hash_length issuer_hash_length,
    caprock_signature_algorithm sigalg
)
{
  // *** Input validation & parsing (because the two often execute
  //     similar paths)
  if (!buffer || !bufsize || !*bufsize) {
    LIBLOG_DEBUG("Output buffer too small or not provided.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // *** Basic token payload
  auto [success, token] = caprock::token_create_v1(
      CTT_REVOKE, issuer, sequence_no,
      from, to, expiry_policy,
      claims, claims_size,
      issuer_hash_length, sigalg
  );
  if (!success) {
    LIBLOG_DEBUG("Could not create valid token from input.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // *** Serialize token into buffer
  char * offset = static_cast<char *>(buffer);
  size_t remaining = *bufsize;

  auto [used, payload] = caprock::codec::v1::encode_token_payload(offset, remaining,
      token.payload);
  if (!used) {
    LIBLOG_DEBUG("Could not serialize token.");
    return CAPROCK_ERR_CODEC;
  }

  // Add signature - the tag is already in place, but the signature itself
  // is missing.
  offset += payload;
  remaining -= payload;

  size_t payload_size = offset - static_cast<char *>(buffer);

  used = caprock::codec::v1::encode_token_signature(offset, remaining,
      token.payload, token.issuer_pair,
      static_cast<char *>(buffer), payload_size);
  if (!used) {
    LIBLOG_DEBUG("Could not add signature.");
    return CAPROCK_ERR_CODEC;
  }

  offset += used;
  remaining -= used;

  *bufsize = offset - static_cast<char *>(buffer);
  return CAPROCK_ERR_SUCCESS;
}



caprock_error_t
caprock_token_validate(
    void const * token, size_t token_size,
    char const * now,
    caprock_key const * verifier)
{
  if (!verifier || !now) {
    LIBLOG_DEBUG("Missing inputs.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // Validate verifier
  if (!caprock_key_is_valid(verifier) || !caprock_key_is_public(verifier)) {
    LIBLOG_DEBUG("Passed key is not a valid public key.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // Parse timestamp
  auto [ts_good, tp] = caprock::util::parseISO8601(now);
  if (!ts_good) {
    LIBLOG_DEBUG("Now date could not be parsed.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  auto [res, _] = parse_and_validate_token(token, token_size, tp, *verifier);
  return res;
}



caprock_error_t
caprock_claim_validate(
    caprock_claim const * claims, size_t claims_size,
    char const * now,
    void * buffer, size_t bufsize,
    caprock_token_iterator iterator,
    void * iterator_baton,
    caprock_key const * verifier,
    int flags
)
{
  if (!claims || !claims_size) {
    LIBLOG_DEBUG("No claim provided.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  if (!now) {
    LIBLOG_DEBUG("No timestamp provided.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  if (!buffer || !bufsize) {
    LIBLOG_DEBUG("No temporary buffer provided.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  if (!iterator) {
    LIBLOG_DEBUG("No token iterator provided.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  if (!verifier || !caprock_key_is_valid(verifier) || !caprock_key_is_public(verifier)) {
    LIBLOG_DEBUG("Passed key is not a valid public key.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  if (flags < 0 || flags > CIF_ALL) {
    LIBLOG_DEBUG("Invalid flag combination.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // Parse timestamp
  auto [ts_good, tp] = caprock::util::parseISO8601(now);
  if (!ts_good) {
    LIBLOG_DEBUG("Now date could not be parsed.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // Since we can have state for multiple claims, we'll use a dynamically
  // allocated structure here. Basically, if a grant token for a claim exists,
  // then we can set the flag, otherwise we have to clear it. For simplicity
  // in the following code, we'll initialize the state here for each claims
  // entry.
  // We'll also validate all claims here; it'll make for a little faster
  // processing later.
  std::map<size_t, bool> state;
  std::vector<caprock::codec::v1::claim_t> validated_claims;
  for (size_t i = 0 ; i < claims_size ; ++i) {
    state[i] = false;

    auto [valid, claim] = caprock::claim_create_v1(claims[i]);
    if (!valid) {
      LIBLOG_DEBUG("An input claim was malformed.");
      return CAPROCK_ERR_INVALID_VALUE;
    }
    validated_claims.push_back(claim);
  }

  // Iterate through tokens. We'll keep track of the last sequence number in
  // order to detect gaps and out-of-order tokens. Why start at the maximum?
  // Because it's very unlikely we'll reach it.
  uint64_t last_sequence_no = std::numeric_limits<uint64_t>::max();

  // Iterate. For each iteration, we'll parse and validate the token.
  do {
    size_t token_size = bufsize;
    auto iter_err = iterator(buffer, &token_size, iterator_baton);
    if (CAPROCK_ERR_ITERATION_END == iter_err) {
      // Nothing more to process.
      break;
    }

    if (CAPROCK_ERR_SUCCESS != iter_err) {
      LIBLOG_DEBUG("Error in iteration.");
      return iter_err;
    }

    // Parse and validate
    auto [err, token] = parse_and_validate_token(buffer, token_size, tp, *verifier);
    if (CAPROCK_ERR_SUCCESS != err) {
      if (flags & CIF_IGNORE_FAILED_SIG) {
        continue;
      }
      LIBLOG_DEBUG("Token validation failed.");
      return err;
    }

    // Check token continuity, except at first token in chain.
    if (last_sequence_no != std::numeric_limits<uint64_t>::max()) {
      // Should be prevented by parse function, but it's a fast enough check.
      if (token.sequence_no == std::numeric_limits<uint64_t>::max()) {
        LIBLOG_DEBUG("Invalid token sequence number, aborting.");
        return CAPROCK_ERR_VALIDATION;
      }

      // Handle out-of-order, if requested
      if (!(flags & CIF_IGNORE_OUT_OF_ORDER)) {
        if (token.sequence_no <= last_sequence_no) {
          LIBLOG_DEBUG("Out-of-order token detected, aborting. Expected "
              "something greater than " << last_sequence_no << " but got "
              << token.sequence_no);
          return CAPROCK_ERR_VALIDATION;
        }
      }

      // Handle gaps, if requested
      if (!(flags & CIF_IGNORE_GAPS)) {
        if (last_sequence_no + 1 != token.sequence_no) {
          LIBLOG_DEBUG("Gap in token chain detected, aborting.");
          return CAPROCK_ERR_VALIDATION;
        }
      }
    }
    last_sequence_no = token.sequence_no;

    // With the basics out of the way, we can check whether this token
    // applies to the given claims.
    for (size_t i = 0 ; i < validated_claims.size() ; ++i) {
      auto & claim = validated_claims[i];
      for (auto & token_claim : token.claims) {
        if (claim == token_claim) {
          // We have a matching claim. Update the state based on whether it's
          // a grant or revoke token.
          switch (token.type) {
            case CTT_GRANT:
              state[i] = true;
              break;

            case CTT_REVOKE:
              state[i] = false;
              break;

            default:
              LIBLOG_DEBUG("Unsupported token type: " << token.type);
              return CAPROCK_ERR_INVALID_VALUE;
          }
        }
      }
    }
  } while (true);

  // Combine all the token states.
  for (auto & [idx, val] : state) {
    if (!val) {
      LIBLOG_DEBUG("Claim at index " << idx << " could not be validated.");
      return CAPROCK_ERR_VALIDATION;
    }
  }

  // All claims granted.
  return CAPROCK_ERR_SUCCESS;
}



CAPROCK_API caprock_error_t
caprock_issuer_from_token(
    void * issuer_buffer, size_t * issuer_buffer_size,
    void const * token_buffer, size_t token_buffer_size
)
{
  // Check inputs
  if (!issuer_buffer || !issuer_buffer_size || !*issuer_buffer_size) {
    LIBLOG_ERROR("Invalid issuer buffer.");
    return CAPROCK_ERR_INVALID_VALUE;
  }
  if (!token_buffer || !token_buffer_size) {
    LIBLOG_ERROR("Invalid token buffer.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // Decode the token
  caprock::codec::v1::token_t parsed;
  auto [used, payload] = caprock::codec::v1::decode_token(parsed,
      static_cast<char const *>(token_buffer), token_buffer_size);
  if (!used) {
    LIBLOG_DEBUG("Unable to parse token.");
    return CAPROCK_ERR_CODEC;
  }

  // If parsing succeeded, we can copy the issuer id_buffer.
  if (parsed.issuer.id_buffer.empty()) {
    LIBLOG_DEBUG("This is unexpected; the issuer ID buffer is empty!");
    return CAPROCK_ERR_VALIDATION;
  }
  if (*issuer_buffer_size < parsed.issuer.id_buffer.size()) {
    LIBLOG_ERROR("Issuer buffer to small for identifier.");
    *issuer_buffer_size = parsed.issuer.id_buffer.size();
    return CAPROCK_ERR_OUT_OF_MEMORY;
  }

  // Copy issuer buffer
  std::memcpy(issuer_buffer, parsed.issuer.id_buffer.data(),
      parsed.issuer.id_buffer.size());
  *issuer_buffer_size = parsed.issuer.id_buffer.size();

  return CAPROCK_ERR_SUCCESS;
}



CAPROCK_API caprock_error_t
caprock_create_object_id(void * buffer, size_t * bufsize,
    char const * name, size_t name_size,
    caprock_identifier_hash_length hash_length)
{
  if (!buffer || !bufsize || *bufsize < (2 + 28) || !name) {
    LIBLOG_DEBUG("Missing input values!");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  if (hash_length == CIH_ANY) {
    LIBLOG_DEBUG("Cannot automatically determine hash length.");
    return CAPROCK_ERR_INVALID_VALUE;
  }

  // Get name length
  size_t input_size = name_size;
  if (!input_size) {
    input_size = strlen(name);
  }

  // Serialize the identifier
  size_t remaining = *bufsize;
  auto used = caprock::serialize_object_identifier_from_name(
      buffer, remaining, name, input_size,
      hash_length);
  if (!used) {
    LIBLOG_DEBUG("Could not encode identifier!");
    return CAPROCK_ERR_CODEC;
  }

  *bufsize = used;
  return CAPROCK_ERR_SUCCESS;
}


} // extern "C"
