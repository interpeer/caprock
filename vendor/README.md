This subdir vendors projects that for one reason or another cannot be pulled
in as meson dependencies or similar.

The subdir's own `meson.build` deals with pulling in appropriate dependencies,
and finally declares a `vendored_deps` variable that can be used in the main
project.
